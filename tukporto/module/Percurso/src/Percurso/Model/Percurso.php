<?php
namespace Percurso\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Percurso implements InputFilterAwareInterface

{

    public $id;

    public $descricao;

    public $data;

    public $turistaid;

    protected $inputFilter;
 // New variable
    public function exchangeArray($data)
    {
        $this->id = (! empty($data['id'])) ? $data['id'] : null;
        $this->descricao = (! empty($data['descricao'])) ? $data['descricao'] : null;
        $this->data = (! empty($data['data'])) ? $data['data'] : null;
        $this->turistaid = (! empty($data['turistaid'])) ? $data['turistaid'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (! $this->inputFilter) {
            $inputFilter = new InputFilter();
            $inputFilter->add(array(
                'name' => 'id',
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'Int'),
                ),
            ));
            $inputFilter->add(array(
                'name' => 'descricao',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ));
            $inputFilter->add(array(
                'name' => 'data',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 100,
                        ),
                    ),
                ),
            ));
          $inputFilter->add(array(
                'name' => 'turistaid',
                'required' => true, 
                'filters' => array(
                    array(
                        'name' => 'Int'),
                ),
            ));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
    
    public function setTurista($turID) {
        $this->turistaid = $turID;
    }
    
    public function setDescricao($desc) {
        $this->descricao = $desc;
    }
    
    public function setData($dat) {
        $this->data = $dat;
    }
    
}
    
