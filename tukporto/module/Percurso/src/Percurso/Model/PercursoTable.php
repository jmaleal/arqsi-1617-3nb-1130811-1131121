<?php
namespace Percurso\Model;

use Zend\Db\TableGateway\TableGateway;

class PercursoTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        
        return $resultSet;
    }

    public function getPercurso($id)
    {
        
        /*
         * $id = (int) $id;
         * $rowSet = $this->tableGateway->select(array(
         * 'id' => $id
         * ));
         * $row->$rowSet->current();
         * if (! $row) {
         * throw new \Exception("Could not find row $id");
         * }
         * return $row;
         */
        $id = (int) $id;
        $rowset = $this->tableGateway->select([
            'id' => $id
        ]);
        $row = $rowset->current();
        if (! $row) {
            
            throw new \Exception("Could not find row $id");
        }
        
        return $row;
    }

    public function savePercurso(Percurso $percurso)
    {
        $cena = $percurso->descricao;//para ver no debug
        $cena2 = $percurso->data; //para ver no debug
        $cena3 = $percurso->turistaid; //para ver no debug
        $data = array(
            'descricao' => $percurso->descricao,
            'data' => $percurso->data,
            'turistaid'=>$percurso->turistaid
        );
        $id = (int) $percurso->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getPercurso($id)) {
                $this->tableGateway->update($data, array(
                    'id' => $id
                ));
            } else {
                throw new \Exception("Percurso id does not exist");
            }
        }
    }

    public function deletePercurso($id)
    {
        $this->tableGateway->delete(array(
            'id' => (int) $id
        ));
    }
    
    public function getCreatorEmail($id)
    {
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception('Percurso id does not exist');
        }
        return $row->id;
    }
 
    
}
    
    

