<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Percurso for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Percurso\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Percurso\Model\Percurso;
use Percurso\Form\PercursoForm;
use Turista\Services\ImoServices;
use Zend\Json\Json;
use PontoPassagem\Model\PontoPassagem;

class PercursoController extends AbstractActionController
{

    protected $percursoTable;

    protected $pontoPassagemTable;

    public function indexAction()
    {
        // vai para login se nao estiver logado
        if (! $this->verificaLogin()) {
            return $this->redirect()->toRoute('turista', array(
                'action' => 'login'
            ));
        }
        
        return new ViewModel(array(
            'percursos' => $this->getPercursoTable()->fetchAll()
        ));
        /*
         * $resultSet = $this->getPercursoTable()->fetchAll();
         * foreach ($resultSet as $result) {
         * echo $result->id;
         * echo $result->descricao;
         *
         * }
         */
        
        /*
         * return array(
         * 'percursos' => $this->getPercursoTable() ->fetchAll(),
         * );
         */
    }

    public function addAction()
    {
        // vai para login se nao estiver logado
        if (! $this->verificaLogin()) {
            return $this->redirect()->toRoute('turista', array(
                'action' => 'login'
            ));
        }
        
        $form = new PercursoForm();
        $form->get('submit')->setValue('Add');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $percurso = new Percurso();
            $form->setInputFilter($percurso->getInputFilter());
            // inlcui turista ID
            
            $data = $request->getPost(); // tira os dados para variavel data
            if (! isset($_SESSION)) {
                session_start();
            }
            
            $data['turistaid'] = $_SESSION['turistaid']; // acrescenta variavel turistaid
            $form->setData($data);
            
            // $form->setData($request->getPost());
            
            if ($form->isValid()) {
                $percurso->exchangeArray($form->getData());
                // adicionei a session para saber qual o turista logado e guardar no turistaid do percurso
                // if (! isset($_SESSION)) {
                // session_start();
                // }
                // $idCriador = $_SESSION['turistaid'];
                
                // if ($idCriador > 0) {
                // $percurso->setTurista($idCriador); //set do turista logado
                // }
                
                // / $data['turistaid'] = $_SESSION['turistaid'];
                // restantes dados do percurso
                
                $this->getPercursoTable()->savePercurso($percurso);
                // Redirect to list of percursos
                return $this->redirect()->toRoute('percurso', array(
                    'action' => 'index'
                ));
            }
        }
        return array(
            'form' => $form
        );
    }

    public function editAction()
    {
        // vai para login se nao estiver logado
        if (! $this->verificaLogin()) {
            return $this->redirect()->toRoute('turista', array(
                'action' => 'login'
            ));
        }
        
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        // Get the Percurso with the specified id. An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $percurso = $this->getPercursoTable()->getPercurso($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        $form = new PercursoForm();
        $form->bind($percurso);
        $form->get('submit')->setAttribute('value', 'Edit');
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $data = $request->getPost(); // tira os dados para variavel data
            
            $percurso->setDescricao($data->descricao);
            $percurso->setData($data->data);
            // $form->setInputFilter($percurso->getInputFilter());
            // $form->setData($request->getPost());
            // if ($form->isValid()) {
            $this->getPercursoTable()->savePercurso($percurso);
            // Redirect to list of percursos
            return $this->redirect()->toRoute('percurso');
            // }
        }
        return array(
            'id' => $id,
            'form' => $form
        );
    }

    public function fooAction()
    {
        return new ViewModel();
    }

    public function detailsAction()
    {
        // vai para login se nao estiver logado
        if (! $this->verificaLogin()) {
            return $this->redirect()->toRoute('turista', array(
                'action' => 'login'
            ));
        }
        
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        // Get the Percurso with the specified id. An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $percurso = $this->getPercursoTable()->getPercurso($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        // return percurso e pontos passagem
        return new ViewModel(array(
            'percurso' => $percurso,
            'pontospassagem' => $this->getPontoPassagemTable()->getPontoPassagemByIDPercurso($id),
            'creator' => $this->getPercursoTable()->getCreatorEmail($id)
        ));
    }

    public function poidetailsAction() // /falta completar
    {
        // vai para login se nao estiver logado
        if (! $this->verificaLogin()) {
            return $this->redirect()->toRoute('turista', array(
                'action' => 'login'
            ));
        }
        
        // id do poi na datum
        $idPercurso = $this->params('id');
        
        try {
            $percurso = $this->getPercursoTable()->getPercurso($idPercurso);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        
        // dois ultimos dias
        $date = str_replace('-', '/', date("Y-m-d"));
        $dia1 = date('Y-m-d', strtotime($date . "-1 days"));
        $dia2 = date('Y-m-d', strtotime($date . "-2 days"));
        
        // todas as leituras dos ultimos 2 dias daquele poi
        $idPoi = $this->params('actionparam1');
        $json = ImoServices::getSensoresDatabyPoiId($idPoi, $dia1, $dia2);
        $var = Json::decode($json);
        
        return new ViewModel(array(
            'poisdata' => Json::decode($json),
            'percurso' => $percurso,
            'idpoi' => $idPoi,
            'nomepoi' => $this->params('actionparam2'),
            'descricaopoi' => $this->params('actionparam3'),
            'latpoi' => $this->params('actionparam4'),
            'longpoi' => $this->params('actionparam5')
        ));
    }

    public function addpontopassagemAction()
    {
        // vai para login se nao estiver logado
        if (! $this->verificaLogin()) {
            return $this->redirect()->toRoute('turista', array(
                'action' => 'login'
            ));
        }
        
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        // Get the Percurso with the specified id. An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $percurso = $this->getPercursoTable()->getPercurso($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('percurso', array(
                'action' => 'index'
            ));
        }
        
        // todos os pois para mostrar
        $json = ImoServices::getAllPois();
        
        // buscar todos os pontos passagens j� adicionados ao percurso
        $pontosPassagemExistentes = $this->getPontoPassagemTable()->getPontoPassagemByIDPercurso($id);
        // return percurso e pontos passagem
        return new ViewModel(array(
            'percurso' => $percurso,
            'pois' => Json::decode($json),
            'pontosexistentes' => $pontosPassagemExistentes
        ));
        // 'creator' => $this->getPercursoTable()->getCreatorEmail($id)
    }

    public function deleteAction()
    {
        // vai para login se nao estiver logado
        if (! $this->verificaLogin()) {
            return $this->redirect()->toRoute('turista', array(
                'action' => 'login'
            ));
        }
        
        $id = (int) $this->params()->fromRoute('id', 0);
        if (! $id) {
            return $this->redirect()->toRoute('percurso');
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');
            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getPercursoTable()->deletePercurso($id);
            }
            // Redirect to list of percursos
            return $this->redirect()->toRoute('percurso');
        }
        return array(
            'id' => $id,
            'percurso' => $this->getPercursoTable()->getPercurso($id)
        );
    }

    public function deletepoiAction()
    {
        // vai para login se nao estiver logado
        if (! $this->verificaLogin()) {
            return $this->redirect()->toRoute('turista', array(
                'action' => 'login'
            ));
        }
        
        $idponto = (int) $this->params('actionparam1');
        if (! $idponto) {
            return $this->redirect()->toRoute('percurso');
        }
        
        $this->getPontoPassagemTable()->deletePonto($idponto);
        
        return $this->redirect()->toRoute('percurso', array(
            'action' => 'addpontopassagem',
            'id' => $this->params('id')
        ));
    }

    public function addpoiAction()
    {
        // vai para login se nao estiver logado
        if (! $this->verificaLogin()) {
            return $this->redirect()->toRoute('turista', array(
                'action' => 'login'
            ));
        }
        
        $ponto = new PontoPassagem();
        $ponto->nome = 'cenas';
        $ponto->nome = $this->params('actionparam1');
        $ponto->descricao = $this->params('actionparam2');
        $ponto->gps_lat = $this->params('actionparam3');
        $ponto->gps_long = $this->params('actionparam4');
        $ponto->percursoid = $this->params('id');
        $ponto->poiid = $this->params('actionparam5');
        
        $this->getPontoPassagemTable()->savePontoPassagem($ponto);
        
        return $this->redirect()->toRoute('percurso', array(
            'action' => 'addpontopassagem',
            'id' => $this->params('id')
        ));
    }

    public function getPercursoTable()
    {
        if (! $this->percursoTable) {
            $sm = $this->getServiceLocator();
            $this->percursoTable = $sm->get('Percurso\Model\PercursoTable');
        }
        return $this->percursoTable;
    }

    public function getPontoPassagemTable()
    {
        $sm = $this->getServiceLocator();
        $pontoPassagemTable = $sm->get('PontoPassagem\Model\PontoPassagemTable');
        
        return $pontoPassagemTable;
    }

    private function verificaLogin()
    {
        if (! isset($_SESSION)) {
            session_start();
        }
        if (empty($_SESSION['turistaid'])) {
            return false;
        }
        return true;
    }
}

