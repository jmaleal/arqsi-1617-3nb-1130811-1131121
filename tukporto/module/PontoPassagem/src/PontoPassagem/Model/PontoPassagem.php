<?php
namespace PontoPassagem\Model;

class PontoPassagem
{

    public $id;

    public $nome;

    public $descricao;

    public $gps_lat;

    public $gps_long;

    public $percursoid;

    public $poiid;

    public function exchangeArray($data)
    {
        $this->id = (! empty($data['id'])) ? $data['id'] : null;
        $this->nome = (! empty($data['nome'])) ? $data['nome'] : null;
        $this->descricao = (! empty($data['descricao'])) ? $data['descricao'] : null;
        $this->gps_lat = (! empty($data['gps_lat'])) ? $data['gps_lat'] : null;
        $this->gps_long = (! empty($data['gps_long'])) ? $data['gps_long'] : null;
        $this->percursoid = (! empty($data['percursoid'])) ? $data['percursoid'] : null;
        $this->poiid = (! empty($data['poiid'])) ? $data['poiid'] : null;
    }
}

