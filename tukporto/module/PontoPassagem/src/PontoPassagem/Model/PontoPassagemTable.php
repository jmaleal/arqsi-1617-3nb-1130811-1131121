<?php
namespace PontoPassagem\Model;

use Zend\Db\TableGateway\TableGateway;


class PontoPassagemTable
{

    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
    
        return $resultSet;
    }
    
    public function getPontoPassagem($id)
    {
    
        /*
         * $id = (int) $id;
         * $rowSet = $this->tableGateway->select(array(
         * 'id' => $id
         * ));
         * $row->$rowSet->current();
         * if (! $row) {
         * throw new \Exception("Could not find row $id");
         * }
         * return $row;
         */
        $id = (int) $id;
        $rowset = $this->tableGateway->select([
            'id' => $id
        ]);
        $row = $rowset->current();
        if (! $row) {
    
            throw new \Exception("Could not find row $id");
        }
    
        return $row;
    }
    
    public function savePontoPassagem(PontoPassagem $pontopassagem)
    {
        $cena = $pontopassagem->descricao;//para ver no debug
        $data = array(
            'nome'=>$pontopassagem->nome,
            'descricao' => $pontopassagem->descricao,
            'gps_lat'=>(double)$pontopassagem->gps_lat,
            'gps_long'=>(double)$pontopassagem->gps_long,
            'percursoid' => (int)$pontopassagem->percursoid,
            'poiid'=>(int)$pontopassagem->poiid
        );
        $id = (int) $pontopassagem->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getPercurso($id)) {
                $this->tableGateway->update($data, array(
                    'id' => $id
                ));
            } else {
                throw new \Exception("Percurso id does not exist");
            }
        }
    }
    
    public function deletePonto($id)
    {
        $this->tableGateway->delete(array(
            'id' => (int) $id
        ));
    }
    
    public function getPontoPassagemByIDPercurso($id)
    {
        $id  = (int) $id;
        $resultSet = $this->tableGateway->select(array('percursoid' => $id));
        return $resultSet;
    }
    
}

