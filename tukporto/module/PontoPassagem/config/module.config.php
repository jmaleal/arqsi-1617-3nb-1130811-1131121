<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'PontoPassagem\Controller\PontoPassagem' => 'PontoPassagem\Controller\PontoPassagemController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'ponto-passagem' => array(
                'type'    => 'segment',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/pontoPassagem[/:action][/:id][/:actionparam1][/:actionparam2][/:actionparam3][/:actionparam4][/:actionparam5][/:actionparam6]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*' ,
                        'id' => '[0-9]+' ,
                    ),
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'PontoPassagem\Controller',
                        'controller'    => 'PontoPassagem',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'PontoPassagem' => __DIR__ . '/../view',
        ),
    ),
);
