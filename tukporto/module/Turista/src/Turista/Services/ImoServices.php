<?php
namespace Turista\Services;

use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Json\Json;
use Turista\DTO\Credenciais;

class ImoServices
{
    
    // *SER� PARA USAR PARA O UTILIZADOR TUKPORTO EM DATUM?????*/
    const URL_Login = '/Token';

    const URL_Values = '/api/values';
    
    // CONST SERVER='WIN-4PVHL23QFU8.wvdomXXX.dei.isep.ipp.pt/Cancela';
    CONST SERVER = 'localhost:44311';

    public static function Login(Credenciais $credenciais)
    {
        if (! isset($_SESSION)) {
            session_start();
        }
        $client = new Client('https://' . self::SERVER . self::URL_Login);
        // $client = new Client('https://localhost:44311/Token');
        
        $client->setMethod(Request::METHOD_POST);
        $params = 'grant_type=password&username=' . $credenciais->email . '&password=' . $credenciais->password;
        
        $len = strlen($params);
        
        $client->setHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Content-Length' => $len
        ));
        
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        $client->setRawBody($params);
        
        $response = $client->send();
        
        $body = Json::decode($response->getBody());
        
        if (! empty($body->access_token)) {
         //  session_start();
            
            $_SESSION['access_token'] = $body->access_token;
            $_SESSION['email'] = $credenciais->email;
            
            return true;
        } else
            return false;
    }

    public static function Logout()
    {
        if (! isset($_SESSION)) {
            session_start();
        }
        
        $_SESSION['email'] = null;
        $_SESSION['access_token'] = null;
    }

    public static function getValues()
    {
        session_start();
        
        $client = new Client('https://' . self::SERVER . self::URL_Values);
        
        $client->setMethod(Request::METHOD_GET);
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        
        $response = $client->send();
        
        $body = $response->getBody();
        
        return $body;
    }

    public static  function getAllPois()
    {
        if (! isset($_SESSION)) {
            session_start();
        }
        
        $var = $_SESSION;
        $client = new Client('https://' . self::SERVER . '/api/pois');
        
        $client->setMethod(Request::METHOD_GET);
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
        
        $client->setOptions([
            'sslverifypeer' => false
        ]);
        
        $response = $client->send();
        
        $body = $response->getBody();
        
        return $body;
    }
    
    public static  function getSensoresDatabyPoiId($idPoi,$dia1,$dia2)
    {
        if (! isset($_SESSION)) {
            session_start();
        }
    
        //$var = $_SESSION;
        
        //$var = 'https://' . self::SERVER . '/api/meteorologias?PoiID='.$idPoi.'&Data_Leiturai='.$dia1.'&Data_Leituraf='.$dia2;
        $client = new Client('https://' . self::SERVER . '/api/meteorologias?PoiID='.$idPoi.'&Data_Leiturai='.$dia2.'&Data_Leituraf='.$dia1);
    
        $client->setMethod(Request::METHOD_GET);
    
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
    
        $client->setHeaders(array(
            'Authorization' => $bearer_token
        ));
    
        $client->setOptions([
            'sslverifypeer' => false
        ]);
    
        $response = $client->send();
    
        $body = $response->getBody();
    
        return $body;
    }
    
    
}

?>