<?php
namespace Turista\Infrastructure\DAL;

use Zend\Db\TableGateway\TableGateway;
use Turista\Model\Turista;

class TuristaTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getTurista($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array(
            'id' => $id
        ));
        $row = $rowset->current();
        if (! $row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getTuristaIdByEmail($email,$pass)
    {
        $rowset = $this->tableGateway->select(array(
            'email' => $email,
            'password'=>$pass
        ));
        $row = $rowset->current();
     /*   if (! $row) {
            throw new \Exception("Could not find row $email");
        }*/
        if(empty($row)){
            return null;
        }
        return $row->id;
    }

    public function saveTurista(Turista $turista)
    {
        $data = array(
            'nome' => $turista->nome,
            'nacionalidade' => $turista->nacionalidade,
            'email' => $turista->email,
            'password' => $turista->password
        );
        
        $id = (int) $turista->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getTurista($id)) {
                $this->tableGateway->update($data, array(
                    'id' => $id
                ));
            } else {
                throw new \Exception('Turista id does not exist');
            }
        }
    }

    public function deleteTurista($id)
    {
        $this->tableGateway->delete(array(
            'id' => (int) $id
        ));
    }
}

?>
