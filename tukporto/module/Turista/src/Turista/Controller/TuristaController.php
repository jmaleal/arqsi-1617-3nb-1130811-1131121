<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Turista for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Turista\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Turista\Form\LoginForm;
use Turista\Form\RegisterForm;
use Turista\Model\Turista;
use Turista\Services\ImoServices;
use Turista\DTO\Credenciais;
use Exception;

class TuristaController extends AbstractActionController
{

    public $turistaTable;

    public function indexAction()
    {
        // return array();
        return new ViewModel();
    }

    public function loginAction()
    {
        $form = new LoginForm();
        $form->get('submit')->setValue('Login');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $form->setData($request->getPost());
            if ($form->isValid()) {
                
                $emailIntroduzido = $_POST["email"]; // obtem a informa��o introduzida no email
                $passIntroduzida = $_POST["password"]; // obtem a informa��o introduzida no email
                
                session_start();
                // procura turista
                $idEncontrado = $this->getTuristaTable()->getTuristaIdByEmail($emailIntroduzido, $passIntroduzida);
                // dados turista encontrado
                
                if ($idEncontrado > 0) { // se � encontrado vai para a lista percursos
                    
                    $_SESSION['turistaid'] = $idEncontrado;
                    $_SESSION['turistaEmail'] = $emailIntroduzido;
                    
                    $this->tukportoLogin(); // efetua a autenticacao do tukporto na
                    if (! empty($_SESSION['access_token'])) {
                        return $this->redirect()->toRoute('turista', array(
                            'action' => 'successlogin'
                        ));
                    }else{
                        $_SESSION['turistaEmail']=null;
                        return $this->redirect()->toRoute('turista', array(
                            'action' => 'login'
                        ));
                    }
                    return $this->redirect()->toRoute('application');
                }
                
                // Redirect to list of turistas
                return $this->redirect()->toRoute('turista');
            }
        }

        return array(
            'form' => $form
        );
    }

    public function successloginAction()
    {
        return new ViewModel();
    }

    public function tukportoLogin()
    {
        ImoServices::Logout();
        $credenciais = new Credenciais();
        $credenciais->exchangeArray(array(
            'email' => 'tukporto@isep.ipp.pt',
            'password' => 'Abc123*'
        ));
        
        while (intval($_SESSION['access_token']) == 0) {
            try {
                ImoServices::Login($credenciais);
            } catch (Exception $e) {
                 //echo "failed token (ocorreu timeout)";
            }
        }
    }

    public function registerAction()
    {
        $form = new RegisterForm();
        $form->get('submit')->setValue('Register');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $turista = new Turista();
            $form->setInputFilter($turista->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $turista->exchangeArray($form->getData());
                $this->getTuristaTable()->saveTurista($turista);
                // Redirect to list of turistas
                return $this->redirect()->toRoute('turista', array(
                    'action' => 'welcome'
                ));
                // return $this->redirect()->toRoute('turista');
            }
        }
        return array(
            'form' => $form
        );
    }

    public function welcomeAction()
    {
        return new ViewModel();
    }

    public function logoutAction()
    {
        // ImoServices::Logout();
        session_start();
        // $_SESSION['turistaEmail']=null;
        session_unset();
        return $this->redirect()->toRoute('turista', array(
            'action' => 'goodbye'
        ));
    }

    public function goodbyeAction()
    {
        return new ViewModel();
    }

    public function getTuristaTable()
    {
        if (! $this->turistaTable) {
            $sm = $this->getServiceLocator();
            $this->turistaTable = $sm->get('Turista\Infrastructure\DAL\TuristaTable');
        }
        return $this->turistaTable;
    }
}
