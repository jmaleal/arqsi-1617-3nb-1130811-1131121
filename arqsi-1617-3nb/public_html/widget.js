/**
 * @author: Carlos Rocha 1131121
 * @author: Jorge Leal 1130811
 */

//var global com dados dos sensores
var sensoresMatriz = [[0,"Meteorologia","Estação meteorologica","Cancela"]];
//var global com dados das facetas para o sensor selecionado
var facetasMatriz = [];

/*
 * Função chamada para carregar widget
 * @returns {ActiveXObject|objXMLHttpRequest|XMLHttpRequest}
 */
function carregaWidget() {

    loadSensoresAPISmartCity();
}

/**
 * Função que carrega os dados dos sensores
 * @returns {undefined}
 */
function loadSensoresAPISmartCity() {
    var objXMLHttpRequest = criarObjectoXMLHttpRequest();
    var url = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/sensores.php";
    objXMLHttpRequest.open("GET", url, true);
    objXMLHttpRequest.onreadystatechange = function () {
        extraiSensores(objXMLHttpRequest,"SmartCity");
        //loadSensoresAPICancela();
        desenharSensores();
    };
    objXMLHttpRequest.send(null);

}

/**
 * Função que carrega os dados dos sensores
 * @returns {undefined}
 */
function loadSensoresAPICancela() {
    var objXMLHttpRequest = criarObjectoXMLHttpRequest();
    var url = "http://localhost:49723/Facetas/IdSensor";
    objXMLHttpRequest.open("GET", url, true);
    objXMLHttpRequest.onreadystatechange = function () {
        extraiSensores(objXMLHttpRequest,"Cancela");
        desenharSensores();
    };
    objXMLHttpRequest.send(null);

}

/*
 * Cria o codigo HTML usando DOM
 * @param {type} objXMLHttpRequest
 * @returns {undefined}
 */
function desenharSensores() {

if (objXMLHttpRequest.readyState === 4 &&
            objXMLHttpRequest.status === 200) {
        //desenho
        var widgetDiv = document.getElementById("widget_vertical");
        //cria div das tabs e append à widget
        var tabsS = document.createElement("div");
        tabsS.setAttribute("id", "tabs");
//criacao da lista de sensores (conforme os detetados no pedido ao servidor)
        var list = document.createElement('ul');
        list.setAttribute('id', 'listaSensores');
        tabsS.appendChild(list);
//cria li para cada sensor
        var idDoSensor = "";
        var prefixo = "tabs-";
        var prefixoRef = "#tabs-";
//        alert(sensoresMatriz.length);
        for (var i = 0; i < sensoresMatriz.length; i++) {

            idDoSensor = sensoresMatriz[i][0];
            //adiciona elemento à lista
            var liElement = document.createElement("li");
            liElement.setAttribute("id", idDoSensor);
            liElement.setAttribute("id", idDoSensor);
            liElement.setAttribute("onclick", "loadFacetasSensor('" + idDoSensor + "')");
            var aElement = document.createElement("a");
            aElement.setAttribute("href", prefixoRef.concat(idDoSensor));
            var nome = document.createTextNode(sensoresMatriz[i][1]);
            aElement.appendChild(nome);
            liElement.appendChild(aElement);
            list.appendChild(liElement);
            //cria div referência para o sensor
            var tabElement = document.createElement("div");
            tabElement.setAttribute("id", prefixo.concat(idDoSensor));
            tabsS.appendChild(tabElement);
            //cria a div para o botao de ocultar/mostrar campos pesquisa
            var divbotaoPesquisa = document.createElement("div");
            //botao
            var botao = document.createElement("BUTTON");
            var idBtn = "btn-pesquisa-" + idDoSensor;
            botao.setAttribute("id", idBtn);
            //acao do botao
            $(document).on("click", "#" + idBtn, function () {
                $(this).parent().next().slideToggle(400);
            });
            var textoDoBtn = document.createTextNode("Pesquisa por Facetas");
            botao.appendChild(textoDoBtn);
            divbotaoPesquisa.appendChild(botao);
            tabElement.appendChild(divbotaoPesquisa);
            //cria a div pesquisa que vai conter as facetas de pesquisa
            var divPesquisaFaceta = document.createElement("div");
            divPesquisaFaceta.setAttribute("id", "pesquisaSensor-" + idDoSensor);
            divPesquisaFaceta.setAttribute("class", "div-pesquisa");

            //cria a div resultados que vai conter a tabela dos resultados
            var divResultadosFaceta = document.createElement("div");
            divResultadosFaceta.setAttribute("class", "div-resultados");
            divResultadosFaceta.setAttribute("id", "resultadosSensor-" + idDoSensor);
            tabElement.appendChild(divPesquisaFaceta);
            tabElement.appendChild(divResultadosFaceta);
        }


        widgetDiv.appendChild(tabsS);
        loadFacetasSensor(sensoresMatriz[0][0]);
        $("#tabs").tabs().addClass("ui-tabs-vertical ui-helper-clearfix");
        $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
        ;
    }  
}

/*
 * Extrai dados dos sensores do objecto XML HTTP Request e grava na variavel sensoresMatriz[]
 * @param {type} objXMLHttpRequest
 * @returns {undefined}
 */
function extraiSensores(objXMLHttpRequest,api) {
if (objXMLHttpRequest.readyState === 4 &&
            objXMLHttpRequest.status === 200) {

    var xml = objXMLHttpRequest.responseXML;
    var sensoresXML = xml.getElementsByTagName("sensor");
    
    var sensoresJaExistentes = sensoresMatriz.length;
    for (i = 0; i < sensoresXML.length; i++) {
//uso de dom e JS para nome sensor e id
        var facetaID = sensoresXML[i].getAttributeNode("id").nodeValue;
        var facetaName = sensoresXML[i].childNodes[0].childNodes[0].nodeValue;
        var facetaDesc = sensoresXML[i].childNodes[1].childNodes[0].nodeValue;
        //guarda informação sensores numa matriz
        sensoresMatriz[i+sensoresJaExistentes] = [];
        sensoresMatriz[i+sensoresJaExistentes][0] = facetaID;
        sensoresMatriz[i+sensoresJaExistentes][1] = facetaName;
        sensoresMatriz[i+sensoresJaExistentes][2] = facetaDesc;
        sensoresMatriz[i+sensoresJaExistentes][3] = api;

    }
    }
}

/**
 * Realiza pedido HTTP extrai dados e cria HTML para as facetas do sensor.
 * @param {type} idSensor
 * @returns {undefined}
 */
function loadFacetasSensor(idSensor) {
    var objXMLHttpRequest = criarObjectoXMLHttpRequest();
    var url="";
    if(sensoresMatriz[idSensor][3]=="SmartCity"){
        url = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/facetas.php?sensor=";
        url = url.concat(idSensor);
    }else if(sensoresMatriz[idSensor][3]=="Cancela"){
        url = "http://localhost:49723/Facetas";
    }
    
    objXMLHttpRequest.open("GET", url, true);
    objXMLHttpRequest.onreadystatechange = function () {
        extraiFacetasSensor(objXMLHttpRequest,idSensor);
        //desenharFaceta(idSensor);
    };
    objXMLHttpRequest.send(null);
}

/*
 * Extrai dados das facetas do objecto XML HTTP Request e grava na variavel facetasMatriz[]
 * @param {type} objXMLHttpRequest
 * @returns {Array|facetasMatriz}
 */
function extraiFacetasSensor(objXMLHttpRequest,idSensor) {

    if (objXMLHttpRequest.readyState === 4 &&
            objXMLHttpRequest.status === 200) {

        var xml = objXMLHttpRequest.responseXML;
        var facetasXML = xml.getElementsByTagName("Faceta");
        facetasMatriz = [];
        for (var i = 0; i < facetasXML.length; i++) {
//uso de dom e JS para nome sensor e id
            var facetaID = facetasXML[i].getAttributeNode("id").nodeValue;
            var facetaBD = facetasXML[i].childNodes[0].childNodes[0].nodeValue;
            var facetaNome = facetasXML[i].childNodes[1].childNodes[0].nodeValue;
            var facetaGrandeza = facetasXML[i].childNodes[2].childNodes[0].nodeValue;
            var facetaTipo = facetasXML[i].childNodes[3].childNodes[0].nodeValue;
            var facetaSemantica = facetasXML[i].childNodes[4].childNodes[0].nodeValue;
            //guarda informação sensores numa matriz
            facetasMatriz[i] = [];
            facetasMatriz[i][0] = facetaID;
            facetasMatriz[i][1] = facetaBD;
            facetasMatriz[i][2] = facetaNome;
            facetasMatriz[i][3] = facetaGrandeza;
            facetasMatriz[i][4] = facetaTipo;
            facetasMatriz[i][5] = facetaSemantica;
        }
        //return facetasMatriz;
        desenharFaceta(idSensor);
    }
    
}

/*
 * Cria codigo HTML para representar as facetas usando DOM
 * @param {type} idSensor
 * @returns {undefined}
 */
function desenharFaceta(idSensor) {
    
    if (objXMLHttpRequest.readyState === 4 &&
            objXMLHttpRequest.status === 200) {
    var pre = "pesquisaSensor-";
    var divPesquisa = document.getElementById(pre.concat(idSensor));
    //apaga tudo o que existir dentro da div
    while (divPesquisa.firstChild) {
        divPesquisa.removeChild(divPesquisa.firstChild);
    }

    var str = "resultadosSensor-";
    str = str.concat(idSensor);
    var divResultados = document.getElementById(str);
    while (divResultados.firstChild) {
        divResultados.removeChild(divResultados.firstChild);
    }

//desenha checkboxes
    var preFaceta = divPesquisa.getAttribute("id").concat("-");
//    alert(matrizFacetas.length);
    for (i = 0; i < facetasMatriz.length; i++) {
        var idFaceta = preFaceta.concat(facetasMatriz[i][0]);
        var divFaceta = document.createElement("div");
        divFaceta.setAttribute("id", idFaceta);
        divPesquisa.appendChild(divFaceta);
        var input = document.createElement("input");
        input.setAttribute("type", "checkbox");
        input.setAttribute("id", facetasMatriz[i][0]);
        divFaceta.appendChild(input);
        divFaceta.appendChild(document.createTextNode(facetasMatriz[i][2]));
    }

//botao para ver resultados
    var botaoR = document.createElement("BUTTON");
    var idBtnR = "btn-resultado-" + idSensor;
    botaoR.setAttribute("id", idBtnR);
    botaoR.setAttribute("class", "btn-resultados");
    botaoR.setAttribute("meta-sensor", idSensor);
    //acao do botao


    //botaoR.setAttribute("onclick","mostraResultados("+idSensor+");");
    var textoDoBtnR = document.createTextNode("Ver Resultados");
    botaoR.appendChild(textoDoBtnR);
    divPesquisa.appendChild(botaoR);
    $('input:checkbox').change(function () {
//identifica qual a faceta
        var iFaceta = 0;
        while (facetasMatriz[iFaceta][0] != $(this).attr("id")) {
            iFaceta++;
        }

        var divParent = $(this).parent();
        //se ainda não foram criados os dados de input..
        if (divParent.children().length <= 1) {

            var divInput = desenhaObjectoTipo(facetasMatriz[iFaceta], idSensor);
            divParent.append(divInput);
            //alert("Check Box " + $(this).attr("id") + " está " + $(this).prop('checked'));
        } else {
            divParent.children().next().slideToggle(400);
        }
//Ver melhor sitio para por este codigo
        $('.datePicker').each(function () {
            $(this).datepicker({dateFormat: 'yy-mm-dd'});
        })

    }
    );
            }
}

/**
 * Cria codigo HTML para representar as facetas respeitando o seu tipo (p.e. data,hora,alfanumerico,ect).
 * @param {type} arrFaceta
 * @param {type} idSensor
 * @returns {tipo.default|tipo|desenhaObjectoTipo.tipo}
 */
function desenhaObjectoTipo(arrFaceta, idSensor) {

    var tipo = {
        'data': function () {
            var divDatePicker = document.createElement("div");
            divDatePicker.setAttribute("id", arrFaceta[1]);
            var par = document.createElement("p");
            divDatePicker.appendChild(par);
            var text = document.createTextNode("De: ");
            par.appendChild(text);
            var datePicker = document.createElement("input");
            datePicker.setAttribute("id", "datePicker1");
            datePicker.setAttribute("class", "datePicker");
            datePicker.setAttribute("type", "text");
            par.appendChild(datePicker);
            if (arrFaceta[3] == "contínuo") {
                var par2 = document.createElement("p");
                divDatePicker.appendChild(par2);
                var text2 = document.createTextNode("Até: ");
                par2.appendChild(text2);
                var datePicker2 = document.createElement("input");
                datePicker2.setAttribute("id", "datePicker2");
                datePicker2.setAttribute("class", "datePicker");
                datePicker2.setAttribute("type", "text");
                par2.appendChild(datePicker2);
            }
            return divDatePicker;
        },
        'alfanumérico': function () {

            var semantica = getSemanticaFacetaById(arrFaceta[0]);

            if (semantica == "coordenadas") {
                var valorDiv = document.createElement("div");
                valorDiv.setAttribute("id", arrFaceta[1]);
                //label para N 
                var par1 = document.createElement("p");
                var label = document.createElement("LABEL");
                var text2 = document.createTextNode("N " + ": ");
                label.appendChild(text2);
                label.setAttribute("for", "valorN");
                par1.appendChild(label);
                //apresenta valor atual
                var val = document.createElement("input");
                var idParaValor = "N" + "-" + idSensor + "-" + arrFaceta[0];
                val.setAttribute("id", idParaValor);
                val.setAttribute("style", "border:0; color:#f6931f; font-weight:bold;");
                par1.appendChild(val);
                valorDiv.appendChild(par1);

                //cria div para o slider
                var slideDivN = document.createElement("div");
                var idSliderN = "slider-rangeN" + "-" + idSensor + "-" + arrFaceta[0];
                slideDivN.setAttribute("id", idSliderN);
                valorDiv.appendChild(slideDivN);

                //label para W 
                var par2 = document.createElement("p");
                var label2 = document.createElement("LABEL");
                var text22 = document.createTextNode("W " + ": ");
                label2.appendChild(text22);
                label2.setAttribute("for", "valorW");
                par2.appendChild(label2);
                //apresenta valor atual
                var val2 = document.createElement("input");
                var idParaValor = "W" + "-" + idSensor + "-" + arrFaceta[0];
                val2.setAttribute("id", idParaValor);
                val2.setAttribute("style", "border:0; color:#f6931f; font-weight:bold;");
                par2.appendChild(val2);
                valorDiv.appendChild(par2);

                //cria div para o slider
                var slideDivW = document.createElement("div");
                var idSliderW = "slider-rangeW" + "-" + idSensor + "-" + arrFaceta[0];
                slideDivW.setAttribute("id", idSliderW);
                valorDiv.appendChild(slideDivW);

                valorDiv.appendChild(slideDivW);


                criaGPSAlfanumerico(idSensor, arrFaceta[0], valorDiv, semantica);
                return valorDiv;

                //criaGPSAlfanumerico(idSensor, arrFaceta[0], valorDiv, semantica);
            } else {
                var divAlfaNum = document.createElement("div");
                divAlfaNum.setAttribute("id", arrFaceta[1]);
                criaFacetaElementsList(idSensor, arrFaceta[0], divAlfaNum);
                return divAlfaNum;
            }

        },
        'hora': function () {
            var horasDiv = document.createElement("div");
            horasDiv.setAttribute("id", arrFaceta[1]);
            var par = document.createElement("p");
            horasDiv.appendChild(par);
            var text = document.createTextNode("Inicio: ");
            par.appendChild(text);
            //combobox horas
            par.appendChild(addCombo("comboHora", "-H-", 24));
            //combobox minutos
            par.appendChild(addCombo("comboHora", "-M-", 60));
            //combobox segundos
            par.appendChild(addCombo("comboHora", "-S-", 60));
            if (arrFaceta[3] == "contínuo") {
                var par2 = document.createElement("p");
                horasDiv.appendChild(par2);
                var text2 = document.createTextNode("Fim: ");
                par2.appendChild(text2);
                par2.appendChild(addCombo("comboHora", "-H-", 24));
                //combobox minutos
                par2.appendChild(addCombo("comboHora", "-M-", 60));
                //combobox segundos
                par2.appendChild(addCombo("comboHora", "-S-", 60));
            }

            return horasDiv;
        },
        'numérico': function () {
            var valorDiv = document.createElement("div");
            valorDiv.setAttribute("id", arrFaceta[1]);
            //label
            var par1 = document.createElement("p");
            var label = document.createElement("LABEL");
            var semantica = getSemanticaFacetaById(arrFaceta[0]);
            var unidade = unidadeApresentacao(semantica);
            var text2 = document.createTextNode("Valor " + unidade + ": ");
            label.appendChild(text2);
            label.setAttribute("for", "valor");
            par1.appendChild(label);
            //apresenta valor atual
            var val = document.createElement("input");
            var idParaValor = "valor" + "-" + idSensor + "-" + arrFaceta[0];
            val.setAttribute("id", idParaValor);
            val.setAttribute("style", "border:0; color:#f6931f; font-weight:bold;");
            if (semantica != "coordenadas") {
                val.readOnly = true;
            }
            par1.appendChild(val);
            //apend à div
            valorDiv.appendChild(par1);
            criaElementosFacetaContinuaNumerica(idSensor, arrFaceta[0], valorDiv, semantica);
            return valorDiv;
        },
        'url': function () {
            var divURL = document.createElement("div");
            divURL.setAttribute("id", arrFaceta[1]);
            var radioS = document.createElement("input");
            radioS.setAttribute("type", "radio");
            radioS.setAttribute("name", "rad");
            radioS.setAttribute("value", "1");
            radioS.setAttribute("id", "1");
            //radioS.appendChild(document.createTextNode("Sim"));
            var radioN = document.createElement("input");
            radioN.setAttribute("type", "radio");
            radioN.setAttribute("name", "rad");
            radioN.setAttribute("value", "0");
            //radioN.appendChild(document.createTextNode("Não"));

            var labelS = document.createElement("label");
            labelS.setAttribute("for", "1");
            labelS.appendChild(document.createTextNode("Sim"));
            var labelN = document.createElement("label");
            labelN.setAttribute("for", "1");
            labelN.appendChild(document.createTextNode("Não"));
            divURL.appendChild(labelS);
            divURL.appendChild(radioS);
            divURL.appendChild(labelN);
            divURL.appendChild(radioN);
            //criaCheckBox(["Sim","Não"], divURL)

            return divURL;
        }
    };
    return (tipo[arrFaceta[4]] || tipo['default'])();
}

/**
 * Realiza pedido HTTP para obter valores possiveis de variaveis discretas.
 * @param {type} idSensor
 * @param {type} idFaceta
 * @param {type} div
 * @returns {undefined}
 */
function criaFacetaElementsList(idSensor, idFaceta, div) {

    var objJSONHttpRequest = criarObjectoXMLHttpRequest();
    if(sensoresMatriz[idSensor][3]=="SmartCity"){
        var url = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/valoresFacetadoSensor.php?sensor=";
        url = url.concat(getSensorNameById(idSensor));
        url = url.concat("&faceta=");
        url = url.concat(getFacetaNameById(idFaceta));
    }else if(sensoresMatriz[idSensor][3]=="Cancela"){
        var url = "http://localhost:49723/Facetas/";
        if(getFacetaNameById(idFaceta) == "NomeLocal")
            url = url.concat("Local");
    }
    
    objJSONHttpRequest.open("GET", url, true);
    objJSONHttpRequest.onreadystatechange = function () {
        if (objJSONHttpRequest.readyState === 4 && objJSONHttpRequest.status === 200) {

            var elements = extrairJSON(objJSONHttpRequest);
            criaMultiSelectionWindow(elements, div, idFaceta);
        }
    };
    objJSONHttpRequest.send(null);
}


/**
 * Cria codigo HTML para janela de multipla selecção.
 * @param {type} elements
 * @param {type} div
 * @param {type} idFaceta
 * @returns {undefined}
 */
function criaMultiSelectionWindow(elements, div, idFaceta) {
    var select = document.createElement("select");
    select.setAttribute("size", "5");
    select.setAttribute("multiple", "multiple");
    var id = "select";
    id = id.concat(idFaceta);
    select.setAttribute("id", id);
    for (i = 0; i < elements.length; i++) {
        var option = document.createElement("option");
        option.setAttribute("value", elements[i]);
        option.appendChild(document.createTextNode(elements[i]));
        select.appendChild(option);
    }
    div.appendChild(select);
}

/**
 * Cria checkbox.
 * @param {type} elements
 * @param {type} div
 * @returns {undefined}
 */
function criaCheckBox(elements, div) {

    for (i = 0; i < elements.length; i++) {
        var input = document.createElement("input");
        input.setAttribute("type", "checkbox");
        input.setAttribute("id", elements[i]);
        div.appendChild(input);
        div.appendChild(document.createTextNode(elements[i]));
    }
}


/**
 * Retorna codigo HTML para criar comboBox
 * @param {type} id
 * @param {type} label
 * @param {type} qtd
 * @returns {addCombo.combo|Element}
 */
function addCombo(id, label, qtd) {

    var combo = document.createElement("select");
    var option;
    var txt;
    option = document.createElement("option");
    option.text = label;
    option.value = label;
    combo.add(option, null); //Standard

    for (i = 0; i < qtd; i++) {
        var optioni = document.createElement("option");
        if (i < 10) {
            optioni.text = "0" + i;
            optioni.value = "0" + i;
        } else {
            optioni.text = i;
            optioni.value = i;
        }
        combo.add(optioni, null); //Standard
    }
    return combo;
}

/**
 * Cria codigo HTML para representar os valores minimos de uma faceta do tipo numerica e continua
 * @param {type} idSensor
 * @param {type} idFaceta
 * @param {type} divParent
 * @param {type} semantica
 * @returns {undefined}
 */
function criaElementosFacetaContinuaNumerica(idSensor, idFaceta, divParent, semantica) {

    /*CRIAR O SLIDER É COMPOSTO POR 2 PARTE
     * a) Ler o valor mínimo (assincronamente espera o valor mínimo e faz novo request para o valor máximo)
     * b) ler o valor máximo (assincrono e portanto só depois de ter o valor máximo é que poderá desenhar o slider)
     */

//PEDIDO PARA LER O VALOR MÍNIMO
    var objJSONHttpRequest = criarObjectoXMLHttpRequest();
    var url="";
     if(sensoresMatriz[idSensor][3]=="SmartCity"){
        url = " http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php?sensor=";
        url = url.concat(getSensorNameById(idSensor));
        url = url.concat("&facetaCont=");
        url = url.concat(getFacetaNameById(idFaceta));
     }else if (sensoresMatriz[idSensor][3]=="Cancela"){
         url = "http://localhost:49723/Facetas/min?faceta=";
         url = url.concat(getFacetaNameById(idFaceta));
     }
    
    objJSONHttpRequest.open("GET", url, true);
    objJSONHttpRequest.onreadystatechange = function () {
        if (objJSONHttpRequest.readyState === 4 && objJSONHttpRequest.status === 200) {

            var stringExtraida = objJSONHttpRequest.response;
            var valorMin = extraiValor(stringExtraida);
            criaElementosFacetaContinuaNumerica2(idSensor, idFaceta, divParent, valorMin, semantica);
        }
    };
    objJSONHttpRequest.send(null);
}


/**
 * Cria codigo HTML para representar os valores maximos de uma faceta do tipo numerica e continua
 * @param {type} idSensor
 * @param {type} idFaceta
 * @param {type} divParent
 * @param {type} valorMin
 * @param {type} semantica
 * @returns {undefined}
 */
function criaElementosFacetaContinuaNumerica2(idSensor, idFaceta, divParent, valorMin, semantica) {
//PEDIDO PARA LER O VALOR MÁXIMO
    var objJSONHttpRequest = criarObjectoXMLHttpRequest();
    var url="";
    if(sensoresMatriz[idSensor][3]=="SmartCity"){
        url = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php?sensor=";
        url = url.concat(getSensorNameById(idSensor));
        url = url.concat("&facetaCont=");
        url = url.concat(getFacetaNameById(idFaceta));
     }else if (sensoresMatriz[idSensor][3]=="Cancela"){
        url = "http://localhost:49723/Facetas/max?faceta=";
         url = url.concat(getFacetaNameById(idFaceta));
     }
    
    objJSONHttpRequest.open("GET", url, true);
    objJSONHttpRequest.onreadystatechange = function () {
        if (objJSONHttpRequest.readyState === 4 && objJSONHttpRequest.status === 200) {

            var stringExtraida = objJSONHttpRequest.response;
            var valorMax = extraiValor(stringExtraida);
            //AGORA COM VALORES MIN E MAX JÁ PODEMOS CRIAR O SLIDE
            var slideDiv = document.createElement("div");
            var idSlider = "slider-range" + "-" + idSensor + "-" + idFaceta;
            slideDiv.setAttribute("id", idSlider);
            divParent.appendChild(slideDiv);
            $(function () {
                $("#" + idSlider).slider({
                    range: true,
                    // min: //definido pelos valores obtidos para a faceta
                    //max: //definido pelos valores obtidos para a faceta
                     //step: 0.01,
                    values: [valorMin, valorMax],
                    slide: function (event, ui) {
                        $("#valor" + "-" + idSensor + "-" + idFaceta).val(ui.values[ 0 ] + ", " + ui.values[ 1 ]);
                    }
                });
            });

//adaptar o step do slider
            if (semantica == "quantidade") { //se for quantidade usar intervalos de numeros inteiros
                $("#" + idSlider).slider("option", "min", parseInt(valorMin));
                $("#" + idSlider).slider("option", "max", parseInt(valorMax));
                $("#" + idSlider).slider("option", "step", 1);
                
                
            } else if (semantica == "coordenadas") {

                var valSplitMin = valorMin.split(".");
                var nCasasDecimaisMin = valSplitMin[1].length;
                var divisorMin= Math.pow(10, nCasasDecimaisMin);

                var valSplitMax = valorMax.split(".");
                var nCasasDecimaisMax = valSplitMax[1].length;
                var divisorMax = Math.pow(10, nCasasDecimaisMax);
                
                var novoMin = parseInt(valorMin * divisorMin) / divisorMin;
                var novoMax = parseInt(valorMax * divisorMax) / divisorMax;
                
                $("#" + idSlider).slider("option", "min", novoMin);
                $("#" + idSlider).slider("option", "max", novoMax);

                $("#" + idSlider).slider("option", "step", 1 / divisorMax);
            } else {
                $("#" + idSlider).slider("option", "min", parseFloat(valorMin));
                $("#" + idSlider).slider("option", "max", parseFloat(valorMax));
                $("#" + idSlider).slider("option", "step", 0.01);
            }
            $("#valor" + "-" + idSensor + "-" + idFaceta).val($("#" + idSlider).slider("values", 0) + ", " + $("#" + idSlider).slider("values", 1));

//            }
        }
    };
    objJSONHttpRequest.send(null);
}


/**
 * Função que faz pedido HTTP e constroi HTML para apresentação dos resultados.
 * @param {type} idSensor
 * @returns {undefined}
 */
function mostraResultados(idSensor) {
//alert("kjk");
    var str = "pesquisaSensor-";
    str = str.concat(idSensor);
    str = str.concat("-");
    if(sensoresMatriz[idSensor][3]=="SmartCity"){
        var url = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/valoresDeSensor.php?sensor=";
        url = url.concat(getSensorNameById(idSensor));
        for (i = 0; i < facetasMatriz.length; i++) {
            var strFaceta = str.concat(facetasMatriz[i][0]);
            var divFaceta = document.getElementById(strFaceta);
            url = url.concat(constroiStringFiltros(divFaceta, facetasMatriz[i]));
        }
    }
    else if(sensoresMatriz[idSensor][3]=="Cancela"){
        var url = "http://localhost:49723/api/Meteorologias?";
        for (i = 0; i < facetasMatriz.length; i++) {
            if(facetasMatriz[i][3]=="discreto"){
                var strFaceta = str.concat(facetasMatriz[i][0]);
                var divFaceta = document.getElementById(strFaceta);
                url = url.concat(stringDiscretosAPICancela(divFaceta, facetasMatriz[i]));
            }
        }
    }
    

    obtemResultados(idSensor, url);
}

/**
 * Retorna string com valores para serem usados como filtro no pedido de obtenção resultados.
 * @param {type} divFaceta
 * @param {type} arrFaceta
 * @returns {unresolved}
 */
function constroiStringFiltros(divFaceta, arrFaceta) {

    var semantica = {
        'data': function () {
            return "";
        },
        'coordenadas': function () {
            return "";
        },
        'hora': function () {
            return "";
        },
        'temperatura': function () {
            return "";
        },
        'localização': function () {
            return stringDiscretos(divFaceta, arrFaceta);
        },
        'monetário': function () {
            return "";
        },
        'organização': function () {
            return stringDiscretos(divFaceta, arrFaceta);
        },
        'qualidade': function () {
            return stringDiscretos(divFaceta, arrFaceta);
        },
        'quantidade': function () {
            return "";
        },
        'foto': function () {
            return "";
        }
    };
    return (semantica[arrFaceta[5]])();
}

/**
 * Realiza pedido HTTP e chama função para criar HTML apresentando os resultados.
 * @param {type} idSensor
 * @param {type} url
 * @returns {undefined}
 */
function obtemResultados(idSensor, url) {
    var objJSONHttpRequest = criarObjectoXMLHttpRequest();
    objJSONHttpRequest.open("GET", url, true);
    objJSONHttpRequest.onreadystatechange = function () {
        if (objJSONHttpRequest.readyState === 4 && objJSONHttpRequest.status === 200) {

            var elements = extrairJSON(objJSONHttpRequest);
            desenhaResultados(elements, idSensor);
        }
    };
    objJSONHttpRequest.send(null);
}

/**
 * Cria codigo HTML para a tabela de resultados populando-a com os dados filtrados.
 * @param {type} json
 * @param {type} idSensor
 * @returns {undefined}
 */
function desenhaResultados(json, idSensor) {
    var str = "resultadosSensor-";
    str = str.concat(idSensor);
    var divResultados = document.getElementById(str);
    while (divResultados.firstChild) {
        divResultados.removeChild(divResultados.firstChild);
    }

    var table = document.createElement("table");
    table.setAttribute("id", "resultados-" + idSensor);
    table.setAttribute("class", "table_resultados");
    divResultados.appendChild(table);
    var columns = addAllColumnHeaders(json, idSensor);
    for (var i = 0; i < json.length; i++) {
        var row$ = $('<tr/>');
        for (var colIndex = 0; colIndex < columns.length; colIndex++) {
            var cellValue = json[i][columns[colIndex]];
            if (cellValue == null) {
                cellValue = "";
            }
            row$.append($('<td/>').html(cellValue));
        }
        if (filtraContinuosNoCliente(idSensor, json[i])) {
            $("#resultados-" + idSensor).append(row$);
        }
    }
}

/**
 * Adiciona nome das facetas ao header da coluna da tabela.
 * @param {type} jsonList
 * @param {type} idSensor
 * @returns {Array|addAllColumnHeaders.columnSet}
 */
function addAllColumnHeaders(jsonList, idSensor) {

    var columnSet = [];
    var headerTr$ = $('<tr/>');
    for (var i = 0; i < jsonList.length; i++) {
        var rowHash = jsonList[i];
        for (var key in rowHash) {
            if ($.inArray(key, columnSet) == -1
                    && keyIsPresent(key)
                    ) {
                columnSet.push(key);
                headerTr$.append($('<th/>').html(key));
            }
        }
    }
    $("#resultados-" + idSensor).append(headerTr$);
    return columnSet;
}

function keyIsPresent(key){
    for (var i=0;i<facetasMatriz.length;i++){
        if(facetasMatriz[i][1]==key){
            return true;
        }
    }
    return false;
}

/**
 * Retorna string com valores selecionados na multiselection window.
 * @param {type} divFaceta
 * @param {type} arrFaceta
 * @returns {stringDiscretos.str|String}
 */
function stringDiscretos(divFaceta, arrFaceta) {
    var str = "";
    var checkBox = divFaceta.childNodes[0];
    if (checkBox.checked) {
        var multiWindow = divFaceta.childNodes[2];
        multiWindow = multiWindow.childNodes[0];
        var nEscolhas = multiWindow.selectedOptions.length;

        if (nEscolhas > 0) {
            str = str.concat("&");
            str = str.concat(arrFaceta[1]);
            str = str.concat("=[");
            var n = 0;
            for (var i = 0; i < multiWindow.selectedOptions.length; i++) {
                if (n > 0) {
                    str = str.concat(",");
                }
                n++;
                str = str.concat(multiWindow.selectedOptions[i].value);
            }
            str = str.concat("]");
        }
    }
    return str;
}

function stringDiscretosAPICancela(divFaceta, arrFaceta) {
    var str = "";
    var checkBox = divFaceta.childNodes[0];
    if (checkBox.checked) {
        str = str.concat(arrFaceta[1]);
        str = str.concat("=");
        var multiWindow = divFaceta.childNodes[2];
        multiWindow = multiWindow.childNodes[0];
        var nEscolhas = multiWindow.selectedOptions.length;

        if (nEscolhas > 0) {
  
            var n = 0;
            for (var i = 0; i < multiWindow.selectedOptions.length; i++) {
                if (n > 0) {
                    str = str.concat(",");
                }
                n++;
                str = str.concat(multiWindow.selectedOptions[i].value);
            }
            
        }
    }
    return str;
}

/**
 * Filtra os valores conforme a sua semantica. Retorna true se valor está 
 * dentro dos filtros, false em casa contrario.
 * @param {type} idSensor
 * @param {type} linhaRes
 * @returns {Boolean}
 */
function filtraContinuosNoCliente(idSensor, linhaRes) {

    var semantica = {
        'na': function () {
            return true;
        },
        'data': function () {

            for (key in linhaRes) {
                if (key == "Data_de_leitura" || key =="Data_Leitura") {
                    //alert(linhaRes["Data_de_leitura"]);
                    var dateCheck = linhaRes[key];
                    var idFaceta = getIdByFacetaName(key);
                    var elementID = "pesquisaSensor-";
                    elementID = elementID.concat(idSensor);
                    elementID = elementID.concat("-");
                    elementID = elementID.concat(idFaceta);
                    var divFaceta = document.getElementById(elementID);
                    var checkBox = divFaceta.childNodes[0];
                    if (checkBox.checked) {
                        var date1 = $(datePicker1).datepicker({dateFormat: 'yyyy-mm-dd'}).val();
                        var date2 = $(datePicker2).datepicker({dateFormat: 'yyyy-mm-dd'}).val();
                        if (dateIsBetween(date1, date2, dateCheck)) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }
            return true;
        },
        'coordenadas': function () {
            var idFaceta = getIdByFacetaName(key);
            var elementID = "pesquisaSensor-";
            elementID = elementID.concat(idSensor);
            elementID = elementID.concat("-");
            elementID = elementID.concat(idFaceta);
            var divFaceta = document.getElementById(elementID);

            var checkBox = divFaceta.childNodes[0];
            if (checkBox.checked) {

                for (key in linhaRes) {

                    if ((key == "GPS_Latitude" || key == "GPS_Lat") && idFaceta == getIdByFacetaName(key)) {
                        var rangeTemp = divFaceta.childNodes[2].childNodes[0].childNodes[1].value;


                        var valorLinha = linhaRes[key] * 1000000;

                        var arrValFiltro = rangeTemp.split(",");
                        var teste = arrValFiltro[0] * 10000;
                        var val1 = parseFloat(arrValFiltro[0] * 1000000);
                        var val2 = parseFloat(arrValFiltro[1] * 1000000);

                        if (valorLinha >= val1 && valorLinha <= val2) {
                            return true;
                        } else {
                            return false;
                        }
                    } else if ((key == "GPS_Longitude" || key == "GPS_Long") && idFaceta == getIdByFacetaName(key)) {
                        var rangeTemp = divFaceta.childNodes[2].childNodes[0].childNodes[1].value;


                        var valorLinha = linhaRes[key] * 1000000;

                        var arrValFiltro = rangeTemp.split(",");
                        var teste = arrValFiltro[0] * 10000;
                        var val1 = parseFloat(arrValFiltro[0] * 1000000);
                        var val2 = parseFloat(arrValFiltro[1] * 1000000);

                        if (valorLinha >= val1 && valorLinha <= val2) {
                            return true;
                        } else {
                            return false;
                        }
                    } else if (key == "GPS" && idFaceta == getIdByFacetaName(key)) {
                        var rangeN = divFaceta.childNodes[2].childNodes[0].childNodes[1].value;
                        var rangeW = divFaceta.childNodes[2].childNodes[2].childNodes[1].value;

                        var valorLinha = linhaRes[key];

                        var arrRangeN = rangeN.split(", ");
                        var rangeN1 = arrRangeN[0];
                        var rangeN2 = arrRangeN[1];

                        var arrRangeW = rangeW.split(", ");
                        var rangeW1 = arrRangeW[0];
                        var rangeW2 = arrRangeW[1];

                        var arrValorLinha = valorLinha.split(";");
                        var valorN = arrValorLinha[0].split("N")[1];
                        var valorW = arrValorLinha[1].split("W")[1];

                        if (valorN >= rangeN1 && valorN <= rangeN2 && valorW >= rangeW1 && valorW <= rangeW2) {
                            return true;
                        } else {
                            return false;
                        }
                    }

                }

                return true;
            }

            return true;
        },
        'hora': function () {
            var idFaceta = getIdByFacetaName(key);
            var elementID = "pesquisaSensor-";
            elementID = elementID.concat(idSensor);
            elementID = elementID.concat("-");
            elementID = elementID.concat(idFaceta);
            var divFaceta = document.getElementById(elementID);

            //so verifica se estiver checked
            var checkBox = divFaceta.childNodes[0];
            if (checkBox.checked) {
                //hora inicio
                var comboHi = divFaceta.childNodes[2].childNodes[0].childNodes[1];
                var valorHi = comboHi.options[comboHi.selectedIndex].value;
                var comboMi = divFaceta.childNodes[2].childNodes[0].childNodes[2];
                var valorMi = comboMi.options[comboMi.selectedIndex].value;
                var comboSi = divFaceta.childNodes[2].childNodes[0].childNodes[3];
                var valorSi = comboSi.options[comboSi.selectedIndex].value;

                //hora fim
                var comboHf = divFaceta.childNodes[2].childNodes[1].childNodes[1];
                var valorHf = comboHf.options[comboHf.selectedIndex].value;
                var comboMf = divFaceta.childNodes[2].childNodes[1].childNodes[2];
                var valorMf = comboMf.options[comboMf.selectedIndex].value;
                var comboSf = divFaceta.childNodes[2].childNodes[1].childNodes[3];
                var valorSf = comboSf.options[comboSf.selectedIndex].value;

                //validação dos valores introduzidos
                var strHoraFiltro = strHoraFiltro + "[" + valorHi + ":" + valorMi + ":" + valorSi + "," + valorHf + ":" + valorMf + ":" + valorSf + "]";
//				if (strHoraFiltro.includes("-")) {
//					alert("Verifique as horas!");
//					return false;
//				}

                //constroi hora inicio
                var horai = new Date();
                horai.setHours(valorHi, valorMi, valorSi);

                //constroi hora fim
                var horaf = new Date();
                horaf.setHours(valorHf, valorMf, valorSf);

                if (horai > horaf) { //se acontecer troca
                    var horatemp = new Date();
                    horatemp.setHours(valorHf, valorMf, valorSf);

                    horaf = horai;
                    horai = horatemp;
                } else {
                    for (key in linhaRes) {
                        if (key == "Hora_de_leitura" || key == "Hora_Leitura") {
                            var valorLinha = linhaRes[key];
                            var arrHoras = valorLinha.split(":");
                            var horalinha = new Date();

                            horalinha.setHours(arrHoras[0], arrHoras[1], arrHoras[2]);

                            if (horalinha >= horai && horalinha <= horaf) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            }

            return true;
        }
        ,
        'temperatura': function () {
            var idFaceta = getIdByFacetaName(key);
            var elementID = "pesquisaSensor-";
            elementID = elementID.concat(idSensor);
            elementID = elementID.concat("-");
            elementID = elementID.concat(idFaceta);
            var divFaceta = document.getElementById(elementID);

            var checkBox = divFaceta.childNodes[0];
            if (checkBox.checked) {
                var rangeTemp = divFaceta.childNodes[2].childNodes[0].childNodes[1].value;

                for (key in linhaRes) {
                    if (key == "Temp") {
                        var valorLinha = linhaRes[key];

                        var arrValFiltro = rangeTemp.split(",");
                        var val1 = parseFloat(arrValFiltro[0]);
                        var val2 = parseFloat(arrValFiltro[1]);

                        if (valorLinha >= val1 && valorLinha <= val2) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }

                return true;
            }

            return true;
        },
        'localização': function () {
            return true;
        },
        'monetário': function () {
            var idFaceta = getIdByFacetaName(key);
            var elementID = "pesquisaSensor-";
            elementID = elementID.concat(idSensor);
            elementID = elementID.concat("-");
            elementID = elementID.concat(idFaceta);
            var divFaceta = document.getElementById(elementID);

            var checkBox = divFaceta.childNodes[0];
            if (checkBox.checked) {
                var rangeTemp = divFaceta.childNodes[2].childNodes[0].childNodes[1].value;

                for (key in linhaRes) {
                    if (key == "Preco") {
                        var valorLinha = linhaRes[key];

                        var arrValFiltro = rangeTemp.split(",");
                        var val1 = parseFloat(arrValFiltro[0]);
                        var val2 = parseFloat(arrValFiltro[1]);

                        if (valorLinha >= val1 && valorLinha <= val2) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }

                return true;
            }

            return true;
        },
        'organização': function () {
            return true;
        },
        'qualidade': function () {
            return true;
        },
        'quantidade': function () {
            
            return semanticaQuantidade("Valor",key,idSensor,linhaRes);
            
        },
        'vento': function () {
            
            return semanticaQuantidade("Vento",key,idSensor,linhaRes);
            
        },
        'humidade': function () {
            
            return semanticaQuantidade("Humidade", key,idSensor,linhaRes);
            
        },
         'pressão': function () {
            
            return semanticaQuantidade("Pressão", key,idSensor,linhaRes);
            
        },
        'no': function () {
            
            return semanticaQuantidade("No",key,idSensor,linhaRes);
            
        },
        'no2': function () {
            
            return semanticaQuantidade("No2",key,idSensor,linhaRes);
            
        },
        'foto': function () {

            var idFaceta = getIdByFacetaName(key);
            var elementID = "pesquisaSensor-";
            elementID = elementID.concat(idSensor);
            elementID = elementID.concat("-");
            elementID = elementID.concat(idFaceta);
            var divFaceta = document.getElementById(elementID);

            var checkBox = divFaceta.childNodes[0];
            if (checkBox.checked) {


                var radioS = divFaceta.childNodes[2].childNodes[1];
                var seSim = radioS.checked;
                var radioN = divFaceta.childNodes[2].childNodes[3];
                var seNao = radioN.checked;

                for (key in linhaRes) {
                    if (key == "Foto") {
                        var urlLinha = linhaRes[key];

                        var tamUrl = urlLinha.length;

                        if (!seSim && !seNao) {
                            return true;
                        } else {
                            if (tamUrl > 0 && seSim) {
                                return true;
                            } else if (tamUrl == 0 && seNao) {
                                return true;
                            }

                        }
                    }
                }
                return false;
            }
            return true;
        }
    };

    for (var key in linhaRes) {
        var sem = getSemanticaByFacetaName(key);
        if (!(semantica[sem]()))
            return false;
    }
    return true;
}

/**
 * Realiza pedido HTTP para saber valores maximos e minimos de GPS do tipo numerico, e cria codigo HTML para representar e filtrar dados.
 * @param {type} idSensor
 * @param {type} idFaceta
 * @param {type} div
 * @param {type} semantica
 * @returns {undefined}
 */
function criaGPSAlfanumerico(idSensor, idFaceta, div, semantica) {
    var objJSONHttpRequest = criarObjectoXMLHttpRequest();
    var url = " http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/minFaceta.php?sensor=";
    url = url.concat(getSensorNameById(idSensor));
    url = url.concat("&facetaCont=");
    url = url.concat(getFacetaNameById(idFaceta));
    objJSONHttpRequest.open("GET", url, true);
    objJSONHttpRequest.onreadystatechange = function () {
        if (objJSONHttpRequest.readyState === 4 && objJSONHttpRequest.status === 200) {

            var stringExtraida = objJSONHttpRequest.response;
            var valorMins = extrai2ValorGPS(stringExtraida);
            criaGPSAlfanumerico2(idSensor, idFaceta, div, valorMins, semantica);
        }
    };
    objJSONHttpRequest.send(null);
}

/**
 * Realiza pedido HTTP para saber valores maximos e minimos de GPS do tipo alfanumerico, e cria codigo HTML para representar e filtrar dados.
 * @param {type} idSensor
 * @param {type} idFaceta
 * @param {type} divParent
 * @param {type} valoresMins
 * @param {type} semantica
 * @returns {undefined}
 */
function criaGPSAlfanumerico2(idSensor, idFaceta, divParent, valoresMins, semantica) {
//PEDIDO PARA LER O VALOR MÁXIMO
    var objJSONHttpRequest = criarObjectoXMLHttpRequest();
    var url = "http://phpdev2.dei.isep.ipp.pt/~arqsi/smartcity/maxFaceta.php?sensor=";
    url = url.concat(getSensorNameById(idSensor));
    url = url.concat("&facetaCont=");
    url = url.concat(getFacetaNameById(idFaceta));
    objJSONHttpRequest.open("GET", url, true);
    objJSONHttpRequest.onreadystatechange = function () {
        if (objJSONHttpRequest.readyState === 4 && objJSONHttpRequest.status === 200) {

            var stringExtraida = objJSONHttpRequest.response;
            var valoresMaxs = extrai2ValorGPS(stringExtraida); //é um array
            //AGORA COM VALORES MIN E MAX JÁ PODEMOS CRIAR OS SLIDES
            //1º SLIDE
            var idSlider = "slider-rangeN" + "-" + idSensor + "-" + idFaceta;

            // FAZER GET ELEMENT COM ID

            $(function () {
                $("#" + idSlider).slider({
                    range: true,
                    // min: //definido pelos valores obtidos para a faceta
                    //max: //definido pelos valores obtidos para a faceta
                    step: 0.01,
                    values: [valoresMins[0], valoresMaxs[0]],
                    slide: function (event, ui) {
                        $("#N" + "-" + idSensor + "-" + idFaceta).val(ui.values[ 0 ] + ", " + ui.values[ 1 ]);
                    }
                });
            });
            $("#" + idSlider).slider("option", "min", parseFloat(valoresMins[0]));
            $("#" + idSlider).slider("option", "max", parseFloat(valoresMaxs[0]));
            var valSplitMin = valoresMins[0].split(".");
            var nCasasDecimais = valSplitMin[1].length;
            var divisor = Math.pow(10, nCasasDecimais);

            $("#" + idSlider).slider("option", "step", 1 / divisor);

            $("#N" + "-" + idSensor + "-" + idFaceta).val($("#" + idSlider).slider("values", 0) + ", " + $("#" + idSlider).slider("values", 1));
            
            //2º SLIDE
            var idSlider2 = "slider-rangeW" + "-" + idSensor + "-" + idFaceta;

            $(function () {
                $("#" + idSlider2).slider({
                    range: true,
                    // min: //definido pelos valores obtidos para a faceta
                    //max: //definido pelos valores obtidos para a faceta
                    step: 0.01,
                    values: [valoresMins[1], valoresMaxs[1]],
                    slide: function (event, ui) {
                        $("#W" + "-" + idSensor + "-" + idFaceta).val(ui.values[ 0 ] + ", " + ui.values[ 1 ]);
                    }
                });
                
            });
            $("#" + idSlider2).slider("option", "min", parseFloat(valoresMins[1]));
            $("#" + idSlider2).slider("option", "max", parseFloat(valoresMaxs[1]));
            var valSplitMax = valoresMaxs[0].split(".");
            var nCasasDecimais2 = valSplitMax[1].length;
            var divisor2 = Math.pow(10, nCasasDecimais2);

            $("#" + idSlider2).slider("option", "step", 1 / divisor2);
            
            $("#W" + "-" + idSensor + "-" + idFaceta).val($("#" + idSlider2).slider("values", 0) + ", " + $("#" + idSlider2).slider("values", 1));

        }
    };
    objJSONHttpRequest.send(null);
}

/**
 * On click do botão mostrar resultados.
 * @param {type} param
 */
$(document).ready(function () {
    $("#widget_vertical").on("click", ".btn-resultados", function () {
//console.log($(this).attr("meta-sensor"));
        mostraResultados($(this).attr("meta-sensor"));
    });
});