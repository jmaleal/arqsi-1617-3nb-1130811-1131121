/**
 * @author: Carlos Rocha 1131121
 * @author: Jorge Leal 1130811
 */


/*
 * Função para criar objecto para realizar um HTTP request
 * @returns {ActiveXObject|objXMLHttpRequest|XMLHttpRequest}
 */
function criarObjectoXMLHttpRequest() {
    if (window.XMLHttpRequest) {
        objXMLHttpRequest = new XMLHttpRequest();
        return objXMLHttpRequest;
    } else if (window.ActiveXObject) {
        objXMLHttpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        return objXMLHttpRequest;
    }
    if (objXMLHttpRequest == null) {
        alert("Erro ao criar o objecto XMLHttpRequest!");
    }
}

/**
 * Extrai dados do tipo JSON, obtidos a partir de XML HTTP request
 * @param {type} objJSONHttpRequest
 * @returns {Array|Object}
 */
function extrairJSON(objJSONHttpRequest) {

    var json = objJSONHttpRequest.response;
    var elements = JSON.parse(json);
    return elements;
}

/**
 * Retorna nome do sensor a partir do seu ID
 * @param {type} sensorID
 * @returns {unresolved}
 */
function getSensorNameById(sensorID) {
    var i = 0;
    while (sensoresMatriz[i][0] != sensorID) {
        i++;
    }
    return sensoresMatriz[i][1];
}

/**
 * Retorna nome da faceta a partir do seu ID
 * @param {type} facetaID
 * @returns {unresolved}
 */
function getFacetaNameById(facetaID) {
    var i = 0;
    while (facetasMatriz[i][0] != facetaID) {
        i++;
    }
    return facetasMatriz[i][1];
}

/**
 * Retorna o ID da faceta a partir do seu nome
 * @param {type} facetaName
 * @returns {unresolved}
 */
function getIdByFacetaName(facetaName) {
    var i = 0;
    while (facetasMatriz[i][1] != facetaName) {
        i++;
    }
    return facetasMatriz[i][0];
}

/**
 * Retorna a semantica da faceta a partir do seu ID
 * @param {type} facetaID
 * @returns {unresolved}
 */
function getSemanticaFacetaById(facetaID) {
    var i = 0;
    while (facetasMatriz[i][0] != facetaID) {
        i++;
    }
    return facetasMatriz[i][5];
}

/**
 * Retorna a semantica da faceta a partir do seu nome
 * @param {type} nomeFaceta
 * @returns {unresolved}
 */
function getSemanticaByFacetaName(nomeFaceta) {
    var i = 0;
    while (i<facetasMatriz.length && facetasMatriz[i][1] != nomeFaceta) {
        i++;
    }
    if(i<facetasMatriz.length){
        return facetasMatriz[i][5];
    }else{
        return "na";
    }
        
}

/**
 * Retorn o tipo da faceta a partir do seu ID.
 * @param {type} facetaID
 * @returns {unresolved}
 */
function getTipoFacetaById(facetaID) {
    var i = 0;
    while (facetasMatriz[i][0] != facetaID) {
        i++;
    }
    return facetasMatriz[i][4];
}

/**
 * Retorna os dados da resposta ao pedido HTTP para valores maixmos e minimos
 * @param {type} stringExtraida
 * @returns {String}
 */
function extraiValor(stringExtraida) {
    var partevalor = stringExtraida.indexOf(':', 0) + 1;
    var startOfValue = stringExtraida.indexOf('"', partevalor) + 1;
    var endOffValue = stringExtraida.indexOf('"', startOfValue); //one char longer, as needed for substring
    var value = stringExtraida.substring(startOfValue, endOffValue);
    var value2 = value.replace(/,/gi, '.');
    if (value2 == "") {
        value2 = "0";
    }
    return value2;
}

/**
 * Retorna a unidade de representação da semantica.
 * @param {type} semantica
 * @returns {String}
 */
function unidadeApresentacao(semantica) {
    var unidade;
    switch (semantica) {

        case "temperatura":
            unidade = "ºC";
            break;
        case "monetário":
            unidade = " €";
            break;
        default:
            unidade = "";
    }

    return unidade;
}

/**
 * Check if date is between two other dates.
 * @param {type} dateFrom
 * @param {type} dateTo
 * @param {type} dateCheck
 * @returns {Boolean}
 */
function dateIsBetween(dateFrom, dateTo, dateCheck) {

    var cutDate = dateCheck.split("T");
    var d1 = dateFrom.split("-");
    var d2 = dateTo.split("-");
    var c = cutDate[0].split("-");
    var from = new Date(d1[0], parseInt(d1[1]) - 1, d1[2]); // -1 because months are from 0 to 11
    var to = new Date(d2[0], parseInt(d2[1]) - 1, d2[2]);
    var check = new Date(c[0], parseInt(c[1]) - 1, c[2]);
    if (from < to) {
        if (check >= from && check <= to) {
            return true;
        }
    } else {
        if (check >= to && check <= from) {
            return true;
        }
    }
    return false;
}

/**
 * Manipula string de modo a conseguir ler numeros da resposta HTTP.
 * @param {type} stringExtraida
 * @returns {Array|extrai2ValorGPS.valoresFinais}
 */
function extrai2ValorGPS(stringExtraida) {
    //1 split
    var partevalor = stringExtraida.indexOf(':', 0) + 1;
    var startOfValue = stringExtraida.indexOf('"', partevalor) + 1;
    var endOffValue = stringExtraida.indexOf('"', startOfValue); //one char longer, as needed for substring
    var value = stringExtraida.substring(startOfValue, endOffValue);
    //2 split
    var valoresFinais = [];
    var partevalor2 = value.split(";");
    var mN = partevalor2[0].substring(1);
    valoresFinais.push(mN);
    var mW = partevalor2[1].substring(1);
    valoresFinais.push(mW);

    return valoresFinais;
}


function semanticaQuantidade(bd_name, key,idSensor,linhaRes){
    
    var idFaceta = getIdByFacetaName(bd_name);
            var elementID = "pesquisaSensor-";
            elementID = elementID.concat(idSensor);
            elementID = elementID.concat("-");
            elementID = elementID.concat(idFaceta);
            var divFaceta = document.getElementById(elementID);

            var checkBox = divFaceta.childNodes[0];
            if (checkBox.checked) {
                var rangeTemp = divFaceta.childNodes[2].childNodes[0].childNodes[1].value;

                for (key in linhaRes) {
                    if (key == bd_name) {
                        var valorLinha = linhaRes[key];

                        var arrValFiltro = rangeTemp.split(",");
                        var val1 = parseInt(arrValFiltro[0]);
                        var val2 = parseInt(arrValFiltro[1]);

                        if (valorLinha >= val1 && valorLinha <= val2) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }

                return true;
            }

            return true;
}