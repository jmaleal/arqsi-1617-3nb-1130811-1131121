﻿using ClassLibrary.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Visita.Helpers
{
    public static class MeteorologiaDTOConverter
    {

        public static Meteorologia convertMet(string content)
        {
            //lista auxiliar para identificar locais repetidos
            SortedList<int, Local> listaLocais = new SortedList<int, Local>();

            //conversao
            var conteudo = JObject.Parse(content);

            var values = conteudo.Values();    //todos os dados de 1 POI

            Meteorologia met = criaMeteorologia(listaLocais, values);


            return met;

        }

        public static IEnumerable<Meteorologia> convertListOfMeteorologia(string content)
        {
            //para devolver
            List<Meteorologia> listaConvertida = new List<Meteorologia>();

            //lista auxiliar para identificar locais repetidos
            SortedList<int, Local> listaLocais = new SortedList<int, Local>();

            //conversao
            var objects = JArray.Parse(content);


            foreach (JObject root in objects)
            {

                var values = root.Values();    //todos os dados de 1 POI

                Meteorologia met = criaMeteorologia(listaLocais, values);

                listaConvertida.Add(met);

            }
            return listaConvertida.AsEnumerable();

        }






        private static Meteorologia criaMeteorologia(SortedList<int, Local> listaLocais, IJEnumerable<JToken> values)
        {
            Meteorologia met = new Meteorologia();

            met.Id = Int32.Parse(values.ElementAt(0).ToString());
            met.Data_Leitura = DateTime.Parse(values.ElementAt(1).ToString());
            met.Hora_Leitura= TimeSpan.Parse(values.ElementAt(2).ToString());
            met.Temp = double.Parse(values.ElementAt(3).ToString());
            met.Vento = double.Parse(values.ElementAt(4).ToString());
            met.Humidade = double.Parse(values.ElementAt(5).ToString());
            met.Pressão = double.Parse(values.ElementAt(6).ToString());
            met.No = double.Parse(values.ElementAt(7).ToString());
            met.No2 = double.Parse(values.ElementAt(8).ToString());

            int codLocal = Int32.Parse(values.ElementAt(9).ToString());
            met.LocalId = codLocal;
            Local local;
            if (!listaLocais.ContainsKey(codLocal))
            {
                local = new Local();
                local.Id = codLocal;
                local.Nome = values.ElementAt(10).ToString();
                local.GPS_Lat = double.Parse(values.ElementAt(11).ToString());
                local.GPS_Long = double.Parse(values.ElementAt(12).ToString());
                listaLocais.Add(codLocal, local);
            }
            else
            {
                int indexLocal = listaLocais.IndexOfKey(codLocal);
                local = listaLocais.Values[indexLocal];
            }
            met.Local = local;
            local.listaMeteorologias.Add(met);

            return met;
        }
    }
}