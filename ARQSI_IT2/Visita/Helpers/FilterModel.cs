﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Visita.Helpers
{
    public class FilterModel
    {
        public int PoiId;
        public DateTime dataFiltro;
        public TimeSpan horaFiltro;
        public int tolerancia;

        public FilterModel(int PoiId, DateTime dataFiltro, TimeSpan horaFiltro, int tolerancia)
        {

            this.PoiId = PoiId;
            this.dataFiltro = dataFiltro;
            this.horaFiltro = horaFiltro;
            this.tolerancia = tolerancia;
        }
    }
}