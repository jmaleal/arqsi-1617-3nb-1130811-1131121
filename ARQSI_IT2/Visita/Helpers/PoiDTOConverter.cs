﻿using ClassLibrary.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Visita.Helpers
{
    public static class PoiDTOConverter
    {

        public static Poi convertPoi(string content)
        {
            //lista auxiliar para identificar locais repetidos
            SortedList<int, Local> listaLocais = new SortedList<int, Local>();

            //conversao
            var conteudo = JObject.Parse(content);

            var values = conteudo.Values();    //todos os dados de 1 POI

            Poi poi = criaPoi(listaLocais, values);


            return poi;

        }


        public static IEnumerable<Poi> convertListOfPois(string content)
        {
            //para devolver
            List<Poi> listaConvertida = new List<Poi>();

            //lista auxiliar para identificar locais repetidos
            SortedList<int, Local> listaLocais = new SortedList<int, Local>();

            //conversao
            var objects = JArray.Parse(content);


            foreach (JObject root in objects)
            {

                var values = root.Values();    //todos os dados de 1 POI

                Poi poi = criaPoi(listaLocais, values);

                listaConvertida.Add(poi);

            }
            return listaConvertida;

        }


        private static Poi criaPoi(SortedList<int, Local> listaLocais, IJEnumerable<JToken> values)
        {
            Poi poi = new Poi();

            poi.Id = Int32.Parse(values.ElementAt(0).ToString());
            poi.Nome = values.ElementAt(1).ToString();
            poi.Descrição = values.ElementAt(2).ToString();


            int codLocal = Int32.Parse(values.ElementAt(3).ToString());
            poi.LocalId = codLocal;
            Local local;
            if (!listaLocais.ContainsKey(codLocal))
            {
                local = new Local();
                local.Id = codLocal;
                local.Nome = values.ElementAt(4).ToString();
                local.GPS_Lat = double.Parse(values.ElementAt(5).ToString());
                local.GPS_Long = double.Parse(values.ElementAt(6).ToString());
                listaLocais.Add(codLocal, local);
            }
            else
            {
                int indexLocal = listaLocais.IndexOfKey(codLocal);
                local = listaLocais.Values[indexLocal];
            }
            poi.Local = local;
            local.listaPois.Add(poi);

            return poi;
        }


    }
}



