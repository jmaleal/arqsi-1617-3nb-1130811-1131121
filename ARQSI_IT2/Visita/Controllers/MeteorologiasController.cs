﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClassLibrary.DAL;
using ClassLibrary.Model;
using Visita.Helpers;
using System.Net.Http;

namespace Visita.Controllers
{
    public class MeteorologiasController : Controller
    {
        private LocalDBContext db = new LocalDBContext();

        // GET: Meteorologias
        public async Task<ActionResult> Index()
        {
            //var meteorologias = db.Meteorologias.Include(m => m.Local);
            //return View(await meteorologias.ToListAsync());

            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Meteorologias");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var mets = MeteorologiaDTOConverter.convertListOfMeteorologia(content);
                //  var  mets= JsonConvert.DeserializeObject<Meteorologia>(content);
                return View(mets);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

        }

        // GET: Meteorologias/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //Meteorologia meteorologia = await db.Meteorologias.FindAsync(id);
            //if (meteorologia == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(meteorologia);


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Meteorologias/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var met = MeteorologiaDTOConverter.convertMet(content);
                //var met = JsonConvert.DeserializeObject<Meteorologia>(content);
                if (met == null) return HttpNotFound();
                return View(met);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }
        // GET: Meteorologias/Filtrar
        public async Task<ActionResult> Filtrar()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Pois");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var pois = PoiDTOConverter.convertListOfPois(content);

                //passagem dos pois para fazer a combobox
                ViewBag.PoiID = pois.Select(e => new SelectListItem
                {
                    Text = e.Nome,
                    Value = e.Id.ToString(),
                });

                //mostrar todos as meteorologias
                var client2 = WebApiHttpClient.GetClient();
                HttpResponseMessage response2 = await client2.GetAsync("api/Meteorologias");
                if (response2.IsSuccessStatusCode)
                {
                    string content2 = await response2.Content.ReadAsStringAsync();
                    var mets = MeteorologiaDTOConverter.convertListOfMeteorologia(content2);

                    //passagem das Meteorologias para fazer a combobox
                    ViewBag.Meteorologias = mets;
                    return View(mets);

                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }
        // GET: Meteorologias/Filtrar
        [HttpPost]
        public async Task<ActionResult> Filtrar(int PoiId, DateTime Data_Leitura, TimeSpan Hora_Leitura, int Margem)
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Pois");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var pois = PoiDTOConverter.convertListOfPois(content);

                //passagem dos pois para fazer a combobox
                ViewBag.PoiID = pois.Select(e => new SelectListItem
                {
                    Text = e.Nome,
                    Value = e.Id.ToString(),
                });

                //mostrar todos as meteorologias
                var client2 = WebApiHttpClient.GetClient();

                string stringDoPedido=construirStringPedidoAPI(PoiId, Data_Leitura,Hora_Leitura, Margem);

                HttpResponseMessage response2 = await client2.GetAsync(stringDoPedido);
                if (response2.IsSuccessStatusCode)
                {
                    string content2 = await response2.Content.ReadAsStringAsync();
                    var mets = MeteorologiaDTOConverter.convertListOfMeteorologia(content2);

                    //passagem das Meteorologias para fazer a combobox
                    ViewBag.Meteorologias = mets;
                    return View(mets);

                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        private string construirStringPedidoAPI(int PoiId, DateTime Data_Leitura, TimeSpan Hora_Leitura, int Margem)
        {
            var stringParaAPI = "api/Meteorologias?PoiID=" + PoiId + "&Data_Leitura="+
                Data_Leitura.Year+"/"+Data_Leitura.Month+"/"+Data_Leitura.Day+ "&Hora_Leitura="+Hora_Leitura+"&Margem="+Margem;
            return stringParaAPI;
            
        }

        // GET: Meteorologias/Create
        public ActionResult Create()
        {
            ViewBag.LocalId = new SelectList(db.Locais, "Id", "Nome");
            return View();
        }

        // POST: Meteorologias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Data_Leitura,Hora_Leitura,Temp,Vento,Humidade,Pressão,No,No2,LocalId")] Meteorologia meteorologia)
        {
            if (ModelState.IsValid)
            {
                db.Meteorologias.Add(meteorologia);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.LocalId = new SelectList(db.Locais, "Id", "Nome", meteorologia.LocalId);
            return View(meteorologia);
        }

        // GET: Meteorologias/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meteorologia meteorologia = await db.Meteorologias.FindAsync(id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocalId = new SelectList(db.Locais, "Id", "Nome", meteorologia.LocalId);
            return View(meteorologia);
        }

        // POST: Meteorologias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Data_Leitura,Hora_Leitura,Temp,Vento,Humidade,Pressão,No,No2,LocalId")] Meteorologia meteorologia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(meteorologia).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.LocalId = new SelectList(db.Locais, "Id", "Nome", meteorologia.LocalId);
            return View(meteorologia);
        }

        // GET: Meteorologias/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Meteorologia meteorologia = await db.Meteorologias.FindAsync(id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            return View(meteorologia);
        }

        // POST: Meteorologias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Meteorologia meteorologia = await db.Meteorologias.FindAsync(id);
            db.Meteorologias.Remove(meteorologia);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
