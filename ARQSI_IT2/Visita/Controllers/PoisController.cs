﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClassLibrary.DAL;
using ClassLibrary.Model;
using Visita.Helpers;
using System.Net.Http;
using Newtonsoft.Json;

namespace Visita.Controllers
{
    public class PoisController : Controller
    {
        private LocalDBContext db = new LocalDBContext();

        // GET: Pois
        public async Task<ActionResult> Index()
        {
            //var pois = db.Pois.Include(p => p.Local);
            //return View(await pois.ToListAsync());


            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Pois");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var pois = PoiDTOConverter.convertListOfPois(content);
               //  var pois = JsonConvert.DeserializeObject<IEnumerable<Poi>>(content);
                return View(pois);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }



        }

        // GET: Pois/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //Poi poi = await db.Pois.FindAsync(id);
            //if (poi == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(poi);


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Pois/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var poi = PoiDTOConverter.convertPoi(content);
                //var poi = JsonConvert.DeserializeObject<Poi>(content);
                if (poi == null) return HttpNotFound();
                return View(poi);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

        }

        // GET: Pois/Create
        public ActionResult Create()
        {
            ViewBag.LocalId = new SelectList(db.Locais, "Id", "Nome");
            return View();
        }

        // POST: Pois/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Nome,Descrição,LocalId")] Poi poi)
        {
            if (ModelState.IsValid)
            {
                db.Pois.Add(poi);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.LocalId = new SelectList(db.Locais, "Id", "Nome", poi.LocalId);
            return View(poi);
        }

        // GET: Pois/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poi poi = await db.Pois.FindAsync(id);
            if (poi == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocalId = new SelectList(db.Locais, "Id", "Nome", poi.LocalId);
            return View(poi);
        }

        // POST: Pois/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Nome,Descrição,LocalId")] Poi poi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(poi).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.LocalId = new SelectList(db.Locais, "Id", "Nome", poi.LocalId);
            return View(poi);
        }

        // GET: Pois/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poi poi = await db.Pois.FindAsync(id);
            if (poi == null)
            {
                return HttpNotFound();
            }
            return View(poi);
        }

        // POST: Pois/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Poi poi = await db.Pois.FindAsync(id);
            db.Pois.Remove(poi);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
