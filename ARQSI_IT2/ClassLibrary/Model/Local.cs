﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Model
{
   public class Local: EntityBase
    {
        public Local()
        {
            listaPois = new List<Poi>();
            listaMeteorologias = new List<Meteorologia>();
        }

        //public new int Id { get; set; }
        [Display(Name = "Nome do Local")]
        public string Nome { get; set; }
        public double GPS_Lat { get; set; }
        public double GPS_Long { get; set; }
        

        //mapeamento para Pois e Metereologias
        public virtual ICollection<Poi> listaPois { get; set; }
        public virtual ICollection<Meteorologia> listaMeteorologias { get; set; }
    }
}
