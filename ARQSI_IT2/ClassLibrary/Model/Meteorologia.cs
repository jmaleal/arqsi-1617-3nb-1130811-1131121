﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Model
{
    public class Meteorologia: EntityBase
    {

        //public new int Id { get; set; }
        public DateTime Data_Leitura { get; set; }
        public TimeSpan Hora_Leitura { get; set; } 
        public double Temp { get; set; }
        public double Vento { get; set; }
        public double Humidade { get; set; }
        public double Pressão { get; set; }
        public double No { get; set; }
        public double No2 { get; set; }

        //para referenciar Local
        public int LocalId { get; set; }
        public virtual Local Local { get; set; }

    }
}
