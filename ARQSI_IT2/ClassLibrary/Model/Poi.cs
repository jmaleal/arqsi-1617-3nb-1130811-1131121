﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ClassLibrary.Model
{
    public class Poi: EntityBase
    {

        //public new int Id { get; set; }
        public string Nome { get; set; }
        public string Descrição { get; set; }

        //para referenciar Local
        public int LocalId { get; set; }
        public virtual Local Local { get; set; }

        //criador
        public string Creator { get; set; }
    }
}
