﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClassLibrary.Model.DTOs
{
    public class MeteorologiaDTO
    {
        public int Id { get; set; }
        public DateTime Data_Leitura { get; set; }
        public TimeSpan Hora_Leitura { get; set; }
        public double Temp { get; set; }
        public double Vento { get; set; }
        public double Humidade { get; set; }
        public double Pressão { get; set; }
        public double No { get; set; }
        public double No2 { get; set; }

        //para referenciar Local
        public int LocalId { get; set; }
        public string NomeLocal{ get; set; }
        public double GPS_Lat { get; set; }
        public double GPS_Long { get; set; }
    }
}