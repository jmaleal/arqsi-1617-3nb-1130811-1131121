﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClassLibrary.Model.DTOs
{
    public class PoiDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descrição { get; set; }

        //para referenciar Local
        public int LocalId { get; set; }
        public string NomeLocal { get; set; }
        public double GPS_Lat { get; set; }
        public double GPS_Long { get; set; }
    }
}