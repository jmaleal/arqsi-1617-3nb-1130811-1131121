﻿namespace ClassLibrary.Model.DTOs
{
    public class LocalDTO
    {
        public LocalDTO()
        {
        }
        public int Id { get; set; }
        public string Nome { get; set; }
        public double GPS_Lat { get; set; }
        public double GPS_Long { get; set; }
    }
}