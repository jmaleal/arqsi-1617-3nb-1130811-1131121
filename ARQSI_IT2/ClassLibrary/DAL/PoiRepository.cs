﻿using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ClassLibrary.DAL
{
    public class PoiRepository : AbsRepository<Poi>
    {
        private LocalDBContext dbContext;

        public PoiRepository(LocalDBContext db) : base(db)
        {
            dbContext = db;
        }


        public LocalDBContext getContext()
        {
            return dbContext;
        }

        /*AQUI COLOCAM-SE OUTROS MÈTODOS QUE NÃO SEJAM CREATE
         *  UPDATE E DELETE, PQ ESSES ESTAO NA SUPERCLASS AbsRepository*/
    }
}

