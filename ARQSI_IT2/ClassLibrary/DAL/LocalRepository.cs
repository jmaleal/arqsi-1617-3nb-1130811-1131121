﻿using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ClassLibrary.DAL
{
    public class LocalRepository : AbsRepository<Local>
    {
        private LocalDBContext dbContext;

        public LocalRepository(LocalDBContext db) : base(db)
        {
            dbContext = db;
        }

        public LocalDBContext getContext()
        {
            return dbContext;
        }



    }
}
