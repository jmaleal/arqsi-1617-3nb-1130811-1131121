﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.IO;
using System.Threading;
using System.Reflection;
using System.Configuration;
using System.Web.Configuration;

namespace ClassLibrary.DAL
{
    public class LocalDBContext: DbContext
    {


        public DbSet<Model.Local> Locais { get; set; }
        public DbSet<Model.Meteorologia> Meteorologias { get; set; }
        public DbSet<Model.Poi> Pois { get; set; }

        //CONSTRUTOR
        public LocalDBContext(): base("LocalDBContext") {

            //Acrescentei para podermos usar o caminho relativo a partir do projeto atual ALTERANDO O |DATADIRETORY|
            string projetoAtual = WebConfigurationManager.AppSettings["NomeProjeto"];
            var asmPath = Thread.GetDomain().BaseDirectory;

            var parentAtual = asmPath;

            while (!parentAtual.EndsWith(projetoAtual))
            {

                parentAtual = Directory.GetParent(parentAtual).ToString();
                var a = parentAtual.Substring(parentAtual.Length - 5);
            }

            var mdfPath = Path.Combine(parentAtual, "ARQSI_IT2\\", "DB");

            AppDomain.CurrentDomain.SetData("DataDirectory", mdfPath);

        }   


    }



}
