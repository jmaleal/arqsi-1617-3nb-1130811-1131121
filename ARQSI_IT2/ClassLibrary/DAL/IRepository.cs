﻿using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DAL
{
    interface IRepository<T> where T : EntityBase
            {
        IEnumerable<T> List();
        IEnumerable<T> List(Expression<Func<T, bool>> predicate);
        T GetbyId(int id);
        T Create(T obj);
        bool Update(T obj);
        bool Delete(T obj);
    }
}
