﻿using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ClassLibrary.DAL
{
    public class MeteorologiaRepository : AbsRepository<Meteorologia>
    {
        private LocalDBContext dbContext { get; set; }

        public MeteorologiaRepository(LocalDBContext db) : base(db)
        {
            dbContext = db;
        }
        /*AQUI COLOCAM-SE OUTROS MÈTODOS QUE NÃO SEJAM CREATE
         *  UPDATE E DELETE, PQ ESSES ESTAO NA SUPERCLASS AbsRepository*/

        public LocalDBContext getContext()
        {
            return dbContext;
        }
    }
}
