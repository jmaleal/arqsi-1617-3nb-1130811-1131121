﻿using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;

namespace ClassLibrary.DAL
{
    public abstract class AbsRepository<T>: IRepository<T> where T: EntityBase
    {
        private readonly DbContext _dbContext;

        public AbsRepository(DbContext db)
        {
            _dbContext = db;
        }

        public IEnumerable<T> List()
        {
            return _dbContext.Set<T>().AsEnumerable();
        }

        public IEnumerable<T> List(Expression<Func<T, bool>> predicate)
        {
            return _dbContext.Set<T>()
           .Where(predicate)
           .AsEnumerable();
        }

        public T GetbyId(int id)
        {
            return _dbContext.Set<T>().Find(id);
        }

        public T Create(T obj)
        {
            _dbContext.Set<T>().Add(obj);
            _dbContext.SaveChanges();
            return obj;
        }

        public bool Update(T obj)
        {

            var ob = _dbContext.Entry(obj);
           if (ob != null)
            {
                ob.State = EntityState.Modified;
                _dbContext.SaveChanges();
                return true;
            }
            else return false;

        }

        public bool Delete(T obj)
        {
            var ob = _dbContext.Entry(obj);
            if (ob != null)
            {
                _dbContext.Set<T>().Remove(obj);
                _dbContext.SaveChanges();
                return true;
            }
            else return false;
        }

        public void dispose()
        {
            _dbContext.Dispose();
        }

    }
}
