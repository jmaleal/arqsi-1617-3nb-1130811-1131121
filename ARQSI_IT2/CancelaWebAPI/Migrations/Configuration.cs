﻿namespace CancelaWebAPI.Migrations
{
    using ClassLibrary.Model;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ClassLibrary.DAL.LocalDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ClassLibrary.DAL.LocalDBContext context)
        {

            //codigo original de exemplo
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );

            //criar roles e users
            criarRolesEUsers();

            ApplicationDbContext context2 = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context2));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context2));
            var users = context2.Users.ToList();

            var editores = users.ToArray();
            var editor1Id = editores[0].UserName;
            var editor2Id = editores[1].UserName;


            //dados para BD
            context.Locais.AddOrUpdate(x => x.Id,
        new Local() { Nome = "Paços de Ferreira", GPS_Lat = 41.278203, GPS_Long = -8.373414 },
        new Local() { Nome = "Porto", GPS_Lat = 41.157944, GPS_Long = -8.629105 },
        new Local() { Nome = "Lisboa", GPS_Lat = 38.722252, GPS_Long = -9.139337 },
        new Local() { Nome = "Funchal", GPS_Lat = 32.666933, GPS_Long = -16.924055 }
        );

            context.Pois.AddOrUpdate(x => x.Id,
                    new Poi { Id = 1, Nome = "Mosteiro de Ferreira", Descrição = "Descrição do Mosteiro", LocalId = 1,Creator=editor1Id },
                    new Poi { Id = 2, Nome = "Citânia Sanfins", Descrição = "Descrição do Mosteiro", LocalId = 1, Creator = editor1Id },
                    new Poi { Id = 3, Nome = "Sé do Porto", Descrição = "Descrição da Sé do Porto", LocalId = 2, Creator = editor1Id },
                    new Poi { Id = 4, Nome = "Casa da Música", Descrição = "Descrição da Casa da Música", LocalId = 2, Creator = editor2Id },
                    new Poi { Id = 5, Nome = "Torre de Belém", Descrição = "Descrição da Torre de Belém", LocalId = 3, Creator = editor2Id },
                   new Poi { Id = 6, Nome = "Pico do Arieiro", Descrição = "Descrição do Pico do Arieiro", LocalId = 4, Creator = editor2Id },
                   new Poi { Id = 7, Nome = "Pico das Torres", Descrição = "Descrição do Pico das Torres", LocalId = 4, Creator = editor2Id }

            );

            context.Meteorologias.AddOrUpdate(x => x.Id,
                    new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-06"), Hora_Leitura = TimeSpan.Parse("17:07:05"), Temp = 15.33, Vento = 40.67, Humidade = 44.67, Pressão = 37.33, No = 21.33, No2 = 22.67, LocalId = 1 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-09-01"), Hora_Leitura = TimeSpan.Parse("22:11:17"), Temp = 7.33, Vento = 36.67, Humidade = 32, Pressão = 37.33, No = 20.67, No2 = 12.67, LocalId = 2 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-09-01"), Hora_Leitura = TimeSpan.Parse("20:50:28"), Temp = 15.33, Vento = 44, Humidade = 32.67, Pressão = 37.33, No = 15.33, No2 = 25.33, LocalId = 4 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-09-01"), Hora_Leitura = TimeSpan.Parse("00:07:35"), Temp = 4.67, Vento = 40, Humidade = 40, Pressão = 46, No = 23.33, No2 = 14, LocalId = 3 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-06"), Hora_Leitura = TimeSpan.Parse("14:25:30"), Temp = 6.67, Vento = 34.67, Humidade = 43.33, Pressão = 46.67, No = 16.67, No2 = 15.33, LocalId = 2 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-06"), Hora_Leitura = TimeSpan.Parse("02:35:21"), Temp = 26.67, Vento = 41.33, Humidade = 34.67, Pressão = 42, No = 18, No2 = 20, LocalId = 1 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-09-02"), Hora_Leitura = TimeSpan.Parse("22:16:01"), Temp = 11.33, Vento = 42, Humidade = 48.67, Pressão = 40.67, No = 22, No2 = 14.67, LocalId = 3 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-06"), Hora_Leitura = TimeSpan.Parse("13:00:58"), Temp = 7.33, Vento = 34.67, Humidade = 51.33, Pressão = 41.33, No = 14.67, No2 = 26.67, LocalId = 2 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-09-03"), Hora_Leitura = TimeSpan.Parse("05:58:04"), Temp = 2.67, Vento = 38.67, Humidade = 40.67, Pressão = 42, No = 14.67, No2 = 15.33, LocalId = 4 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-06"), Hora_Leitura = TimeSpan.Parse("03:14:04"), Temp = 8, Vento = 34.67, Humidade = 37.33, Pressão = 45.33, No = 21.33, No2 = 15.33, LocalId = 1 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-09-03"), Hora_Leitura = TimeSpan.Parse("04:12:36"), Temp = 26, Vento = 43.33, Humidade = 51.33, Pressão = 40, No = 18, No2 = 26.67, LocalId = 2 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-09-03"), Hora_Leitura = TimeSpan.Parse("01:25:43"), Temp = 23.33, Vento = 44, Humidade = 48.67, Pressão = 46.67, No = 18, No2 = 14.67, LocalId = 3 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-09-04"), Hora_Leitura = TimeSpan.Parse("17:59:10"), Temp = 6.67, Vento = 44.67, Humidade = 41.33, Pressão = 46, No = 14, No2 = 25.33, LocalId = 4 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-09-04"), Hora_Leitura = TimeSpan.Parse("02:30:39"), Temp = 7.33, Vento = 40, Humidade = 26, Pressão = 46, No = 20.67, No2 = 22.67, LocalId = 1 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-09-04"), Hora_Leitura = TimeSpan.Parse("08:35:05"), Temp = 21.33, Vento = 43.33, Humidade = 52.67, Pressão = 42.67, No = 23.33, No2 = 18.67, LocalId = 4 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-09-04"), Hora_Leitura = TimeSpan.Parse("08:35:05"), Temp = 21.33, Vento = 43.33, Humidade = 52.67, Pressão = 42.67, No = 23.33, No2 = 18.67, LocalId = 4 },
          new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-15"), Hora_Leitura = TimeSpan.Parse("17:07:05"), Temp = 4.67, Vento = 6, Humidade = 3.33, Pressão = 5.33, No = 9.67, No2 = 12, LocalId = 4 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-13"), Hora_Leitura = TimeSpan.Parse("22:11:17"), Temp = 10.67, Vento = 11, Humidade = 11, Pressão = 12, No = 8.33, No2 = 1.67, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-12"), Hora_Leitura = TimeSpan.Parse("20:50:28"), Temp = 5.67, Vento = 8, Humidade = 7, Pressão = 9.67, No = 5.67, No2 = 11.33, LocalId = 3 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-12"), Hora_Leitura = TimeSpan.Parse("00:07:35"), Temp = 2.67, Vento = 6.67, Humidade = 12, Pressão = 2.33, No = 10.33, No2 = 7, LocalId = 3 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-15"), Hora_Leitura = TimeSpan.Parse("14:25:30"), Temp = 11.67, Vento = 9, Humidade = 10.33, Pressão = 12.33, No = 5.67, No2 = 3.33, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-14"), Hora_Leitura = TimeSpan.Parse("02:35:21"), Temp = 9, Vento = 1.67, Humidade = 1.67, Pressão = 6.33, No = 5.67, No2 = 4, LocalId = 3 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-15"), Hora_Leitura = TimeSpan.Parse("22:16:01"), Temp = 2, Vento = 6.33, Humidade = 8.67, Pressão = 12.67, No = 3.67, No2 = 6, LocalId = 4 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-14"), Hora_Leitura = TimeSpan.Parse("13:00:58"), Temp = 9, Vento = 9.33, Humidade = 2.67, Pressão = 5.33, No = 3.67, No2 = 2, LocalId = 3 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-12"), Hora_Leitura = TimeSpan.Parse("05:58:04"), Temp = 12.67, Vento = 7.33, Humidade = 6.33, Pressão = 4, No = 5.67, No2 = 7.33, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-13"), Hora_Leitura = TimeSpan.Parse("03:14:04"), Temp = 8, Vento = 2, Humidade = 10, Pressão = 5.67, No = 6.67, No2 = 3.67, LocalId = 3 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-13"), Hora_Leitura = TimeSpan.Parse("04:12:36"), Temp = 7, Vento = 12.33, Humidade = 2.67, Pressão = 5.67, No = 9.33, No2 = 4.67, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-14"), Hora_Leitura = TimeSpan.Parse("01:25:43"), Temp = 3.67, Vento = 2.67, Humidade = 5.33, Pressão = 3.67, No = 3.67, No2 = 11.67, LocalId = 4 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-14"), Hora_Leitura = TimeSpan.Parse("17:59:10"), Temp = 2.33, Vento = 7.33, Humidade = 3.33, Pressão = 5, No = 4.67, No2 = 3, LocalId = 4 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-16"), Hora_Leitura = TimeSpan.Parse("02:30:39"), Temp = 10, Vento = 8.67, Humidade = 11.67, Pressão = 9, No = 3.67, No2 = 12, LocalId = 2 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-15"), Hora_Leitura = TimeSpan.Parse("08:35:05"), Temp = 8.33, Vento = 3.33, Humidade = 11, Pressão = 6.67, No = 4.33, No2 = 9, LocalId = 3 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-15"), Hora_Leitura = TimeSpan.Parse("01:06:40"), Temp = 7, Vento = 8, Humidade = 5.67, Pressão = 12.67, No = 8, No2 = 2.33, LocalId = 4 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-12"), Hora_Leitura = TimeSpan.Parse("18:32:54"), Temp = 2.33, Vento = 5.33, Humidade = 7.33, Pressão = 10.67, No = 11.33, No2 = 6.67, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-16"), Hora_Leitura = TimeSpan.Parse("14:54:12"), Temp = 6.33, Vento = 6.33, Humidade = 10.33, Pressão = 2.33, No = 5.67, No2 = 3.67, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-14"), Hora_Leitura = TimeSpan.Parse("12:22:25"), Temp = 6, Vento = 9.67, Humidade = 11.67, Pressão = 11.67, No = 6, No2 = 6, LocalId = 4 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-12"), Hora_Leitura = TimeSpan.Parse("22:15:53"), Temp = 8.67, Vento = 10, Humidade = 5.67, Pressão = 11.33, No = 11.67, No2 = 6.67, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-14"), Hora_Leitura = TimeSpan.Parse("02:29:45"), Temp = 6.67, Vento = 8.33, Humidade = 11.67, Pressão = 2, No = 12.67, No2 = 8, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-12"), Hora_Leitura = TimeSpan.Parse("02:12:34"), Temp = 8, Vento = 9.67, Humidade = 11.67, Pressão = 11.33, No = 4.33, No2 = 5.67, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-14"), Hora_Leitura = TimeSpan.Parse("09:29:49"), Temp = 3.67, Vento = 2.33, Humidade = 9, Pressão = 6, No = 3.33, No2 = 6.33, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-12"), Hora_Leitura = TimeSpan.Parse("00:40:41"), Temp = 5, Vento = 2.33, Humidade = 8.33, Pressão = 6.67, No = 12.67, No2 = 4.33, LocalId = 2 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-13"), Hora_Leitura = TimeSpan.Parse("02:01:29"), Temp = 12.67, Vento = 9, Humidade = 4.33, Pressão = 9, No = 2.33, No2 = 2.33, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-15"), Hora_Leitura = TimeSpan.Parse("23:27:13"), Temp = 8.33, Vento = 2.67, Humidade = 9, Pressão = 2.67, No = 5, No2 = 3, LocalId = 4 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-14"), Hora_Leitura = TimeSpan.Parse("00:30:06"), Temp = 11.67, Vento = 3.67, Humidade = 11.33, Pressão = 12.67, No = 3.67, No2 = 1.67, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-15"), Hora_Leitura = TimeSpan.Parse("19:33:52"), Temp = 4, Vento = 10.67, Humidade = 7.67, Pressão = 7, No = 5.67, No2 = 8, LocalId = 4 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-16"), Hora_Leitura = TimeSpan.Parse("06:52:12"), Temp = 12.33, Vento = 3, Humidade = 2, Pressão = 7.33, No = 1.67, No2 = 10.33, LocalId = 4 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-16"), Hora_Leitura = TimeSpan.Parse("05:05:48"), Temp = 4.33, Vento = 9.67, Humidade = 6, Pressão = 12.33, No = 3, No2 = 3.33, LocalId = 2 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-16"), Hora_Leitura = TimeSpan.Parse("07:44:21"), Temp = 11.33, Vento = 9, Humidade = 11.67, Pressão = 4, No = 6, No2 = 10.33, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-16"), Hora_Leitura = TimeSpan.Parse("10:47:40"), Temp = 7, Vento = 10.67, Humidade = 12.67, Pressão = 11.67, No = 10, No2 = 3, LocalId = 1 },
new Meteorologia { Data_Leitura = DateTime.Parse("2016-12-15"), Hora_Leitura = TimeSpan.Parse("13:32:19"), Temp = 8, Vento = 10.33, Humidade = 5, Pressão = 8.33, No = 8.67, No2 = 2.67, LocalId = 2 }

              );

            criarUserTukporto();
                        
        }

        private void criarUserTukporto()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            //Tukporto
            var tuk = new ApplicationUser();
            tuk.UserName = "tukporto@isep.ipp.pt";
            tuk.Email = "tukporto@isep.ipp.pt";

            string tukpass = "Abc123*";

            var chkUser3 = UserManager.Create(tuk, tukpass);
        }

        private void criarRolesEUsers()
        {

            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


                      // role Editor 
            if (!roleManager.RoleExists("Editor"))
            {

                // Criar o Role  Editor 
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Editor";
                roleManager.Create(role);


                //Criar 2 editores
                //editor1 
                var user = new ApplicationUser();
                user.UserName = "editor1@hotmail.com";
                user.Email = "editor1@hotmail.com";

                string userPWD = "Abc123*";

                var chkUser = UserManager.Create(user, userPWD);

                //atribuir role ao user
                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "Editor");

                }

                //editor2
                var user2 = new ApplicationUser();
                user2.UserName = "editor2@hotmail.com";
                user2.Email = "editor2@hotmail.com";

                string userPWD2 = "Abc123*";

                var chkUser2 = UserManager.Create(user2, userPWD2);

                //atribuir role ao user
                if (chkUser2.Succeeded)
                {
                    var result2 = UserManager.AddToRole(user2.Id, "Editor");

                }


            }

            //// criar o Role diferente para o barramento de certas operacoes
            //if (!roleManager.RoleExists("Registado"))
            //{
            //    var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
            //    role.Name = "Registado";
            //    roleManager.Create(role);


            //    //Criar utilizadores registados normais que n�o ter�o permissoes
            //    //registado1 
            //    var user = new ApplicationUser();
            //    user.UserName = "registado1@hotmail.com";
            //    user.Email = "registado1@hotmail.com";

            //    string userPWD = "Abc123*";

            //    var chkUser = UserManager.Create(user, userPWD);

            //    //atribuir role ao user
            //    if (chkUser.Succeeded)
            //    {
            //        var result1 = UserManager.AddToRole(user.Id, "Registado");

            //    }


            //    //registado2
            //    var user2 = new ApplicationUser();
            //    user2.UserName = "registado2@hotmail.com";
            //    user2.Email = "registado2@hotmail.com";

            //    string userPWD2 = "Abc123*";

            //    var chkUser2 = UserManager.Create(user2, userPWD2);

            //    //atribuir role ao user
            //    if (chkUser2.Succeeded)
            //    {
            //        var result2 = UserManager.AddToRole(user2.Id, "Registado");

            //    }


            //}



        }
    }
}
