﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Configuration;
using System.Threading;
using System.IO;
using System;

namespace CancelaWebAPI.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("LocalDBContext", throwIfV1Schema: false)
        {
        }
        
        public static ApplicationDbContext Create()
        {
            //Acrescentei para podermos usar o caminho relativo a partir do projeto atual ALTERANDO O |DATADIRETORY|
            string projetoAtual = WebConfigurationManager.AppSettings["NomeProjeto"];
            var asmPath = Thread.GetDomain().BaseDirectory;

            var parentAtual = asmPath;

            while (!parentAtual.EndsWith(projetoAtual))
            {

                parentAtual = Directory.GetParent(parentAtual).ToString();
                var a = parentAtual.Substring(parentAtual.Length - 5);
            }

            var mdfPath = Path.Combine(parentAtual, "ARQSI_IT2\\", "DB");

            AppDomain.CurrentDomain.SetData("DataDirectory", mdfPath);
            return new ApplicationDbContext();
        }
    }
}