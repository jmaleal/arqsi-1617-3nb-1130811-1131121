﻿using System;
using Microsoft.Owin;
using Owin;
using System.Threading;
using System.IO;

[assembly: OwinStartup(typeof(CancelaWebAPI.Startup))]

namespace CancelaWebAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
