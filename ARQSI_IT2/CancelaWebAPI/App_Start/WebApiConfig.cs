﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using System.Web.Mvc;

namespace CancelaWebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            //Remove exception of ReferenceLoopHandling
            //config.Formatters.JsonFormatter
            //.SerializerSettings
            //.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            //GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //config.Routes.MapHttpRoute(
            //    name: "Facetas",
            //    routeTemplate: "api/{controller}/FacetasXML",
            //    defaults: new { action = "FacetasXML"}
            //);

            config.Routes.MapHttpRoute(
                name: "Facetas",
                routeTemplate: "api/{controller}/FacetasXML",
                defaults: new { action = "FacetasXML" }
            );

            config.Routes.MapHttpRoute(
                name: "Facetas/min",
                routeTemplate: "api/{controller}/",
                defaults: new { action = "continuoMin" }
            );

            config.Routes.MapHttpRoute(
                name: "Facetas/max",
                routeTemplate: "api/{controller}/",
                defaults: new { action = "continuoMax" }
            );

            config.Routes.MapHttpRoute(
               name: "Facetas/Local",
               routeTemplate: "api/{controller}/",
               defaults: new { action = "LocaisComMeteorologias" }
           );
            config.Routes.MapHttpRoute(
              name: "Facetas/IdSensor",
              routeTemplate: "api/{controller}/",
              defaults: new { action = "FacetaIdSensor" }
          );

        }
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            // novo, referindo a classe criada no passo anterior
            filters.Add(new CancelaWebAPI.Filters.RequireHttpsAttribute());
        }
    }

}
