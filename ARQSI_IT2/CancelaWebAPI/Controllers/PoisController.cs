﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;
using ClassLibrary.Model;
using CancelaWebAPI.Controllers.DTOs;
using CancelaWebAPI.Filters;
using Microsoft.AspNet.Identity;
using System.Web;
using System.Diagnostics;

namespace CancelaWebAPI.Controllers
{
    [Authorize]    //colocando aqui (no inicio da classe) a anotacao todos os metodos partilham esta anotacao
    [RequireHttps] //colocando aqui (no inicio da classe) a anotacao todos os metodos partilham esta anotacao
    public class PoisController : ApiController
    {
        private LocalDBContext db = new LocalDBContext();

        // GET: api/Pois
        public IQueryable<PoiDTO> GetPois()
        {
            //return db.Pois;

            var pois = from poi in db.Pois
                         select new PoiDTO()
                         {
                             Id = poi.Id,
                             Nome = poi.Nome,
                             Descricao = poi.Descrição,
                             LocalId = poi.LocalId,
                             NomeLocal = poi.Local.Nome,
                             GPS_Lat = poi.Local.GPS_Lat,
                             GPS_Long = poi.Local.GPS_Long,
                             Creator = poi.Creator
                         };

            return pois;
        }

        // GET: api/Pois/5
        [ResponseType(typeof(PoiDTO))]
        public async Task<IHttpActionResult> GetPoi(int id)
        {
            Poi poi = await db.Pois.FindAsync(id);

            var poiDTO = new PoiDTO()
            {
                Id = poi.Id,
                Nome = poi.Nome,
                Descricao = poi.Descrição,
                LocalId = poi.Local.Id,
                NomeLocal = poi.Local.Nome,
                GPS_Lat = poi.Local.GPS_Lat,
                GPS_Long = poi.Local.GPS_Long,
                Creator = poi.Creator
            };

            //var poi = await db.Pois.Include(p => p.Local).Select(p =>
            //new PoiDTO()
            //{
            //    Nome = p.Nome,
            //    Descrição = p.Descrição,
            //    LocalId = p.Local.Id,
            //    NomeLocal = p.Local.Nome,
            //    GPS_Lat = p.Local.GPS_Lat,
            //    GPS_Long = p.Local.GPS_Long
            //}).SingleOrDefaultAsync(p => p.Id == id);

            if (poiDTO == null)
            {
                return NotFound();
            }

            return Ok(poiDTO);
        }

        // PUT: api/Pois/5
        [Authorize(Roles = "Editor")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPoi(int id, Poi poi)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != poi.Id)
            {
                return BadRequest();
            }
            var poiCreator = db.Pois.Where(i => i.Id == id).Select(p=>p.Creator).First();
            if (!poiCreator.Equals(User.Identity.GetUserName()))
            {
                return BadRequest("U must be the creator!");
            }
            poi.Creator = poiCreator;
            db.Entry(poi).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PoiExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Pois
        [Authorize(Roles = "Editor")]
        [ResponseType(typeof(Poi))]
        public async Task<IHttpActionResult> PostPoi(Poi poi)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //adiciona o user como criador
            poi.Creator= User.Identity.GetUserName();

            db.Pois.Add(poi);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = poi.Id }, poi);
        }

        // DELETE: api/Pois/5
        [Authorize(Roles = "Editor")]
        [ResponseType(typeof(Poi))]
        public async Task<IHttpActionResult> DeletePoi(int id)
        {
            Poi poi = await db.Pois.FindAsync(id);
            if (poi == null)
            {
                return NotFound();
            }

            if (!poi.Creator.Equals(User.Identity.GetUserName()))
            {
                return BadRequest("U must be the creator!");
            }

            db.Pois.Remove(poi);
            await db.SaveChangesAsync();

            return Ok(poi);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PoiExists(int id)
        {
            return db.Pois.Count(e => e.Id == id) > 0;
        }
    }
}