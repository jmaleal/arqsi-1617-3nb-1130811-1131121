﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.DAL;
using ClassLibrary.Model;
using CancelaWebAPI.Controllers.DTOs;
using System.Collections.Generic;
using CancelaWebAPI.Filters;

namespace CancelaWebAPI.Controllers
{
    [Authorize]     //colocando aqui (no inicio da classe) a anotacao todos os metodos partilham esta anotacao
    [RequireHttps] //colocando aqui (no inicio da classe) a anotacao todos os metodos partilham esta anotacao
    public class MeteorologiasController : ApiController
    {
        private LocalDBContext db = new LocalDBContext();

        // GET: api/Meteorologias
        [Authorize(Roles = "Editor")]
        public IQueryable<MeteorologiaDTO> GetMeteorologias()
        {
            //return db.Meteorologias;

            var meteos = from meteo in db.Meteorologias
                         select new MeteorologiaDTO()
                         {
                             Id = meteo.Id,
                             Data_Leitura = meteo.Data_Leitura,
                             Hora_Leitura = meteo.Hora_Leitura,
                             Temp = meteo.Temp,
                             Vento = meteo.Vento,
                             Humidade = meteo.Humidade,
                             Pressão = meteo.Pressão,
                             No = meteo.No,
                             No2 = meteo.No2,
                             LocalId = meteo.LocalId,
                             NomeLocal = meteo.Local.Nome,
                             GPS_Lat = meteo.Local.GPS_Lat,
                             GPS_Long = meteo.Local.GPS_Long
                         };
            return meteos;
        }

        // GET: api/Meteorologias/5
        [Authorize(Roles = "Editor")]
        [ResponseType(typeof(MeteorologiaDTO))]
        public async Task<IHttpActionResult> GetMeteorologia(int id)
        {
            Meteorologia meteo = await db.Meteorologias.FindAsync(id);

            var meteoDTO = new MeteorologiaDTO()
            {
                Id = meteo.Id,
                Data_Leitura = meteo.Data_Leitura,
                Hora_Leitura = meteo.Hora_Leitura,
                Temp = meteo.Temp,
                Vento = meteo.Vento,
                Humidade = meteo.Humidade,
                Pressão = meteo.Pressão,
                No = meteo.No,
                No2 = meteo.No2,
                LocalId = meteo.LocalId,
                NomeLocal = meteo.Local.Nome,
                GPS_Lat = meteo.Local.GPS_Lat,
                GPS_Long = meteo.Local.GPS_Long
            };
            if (meteoDTO == null)
            {
                return NotFound();
            }

            return Ok(meteoDTO);
        }

        // GET: api/Meteorologias    ex:/api/Meteorologias?PoiID=1&Data_Leitura=‎2016-11-10&Hora_Leitura=12:00
        [ResponseType(typeof(MeteorologiaDTO))]
        public IQueryable<MeteorologiaDTO> GetMeteorologia(int PoiId, DateTime Data_Leiturai, DateTime Data_Leituraf)
        {

            Poi poi = db.Pois.Where(p => p.Id == PoiId).Single();


            //filtragem;
            var mets = from meteo in db.Meteorologias.ToList()
                       where (meteo.LocalId == poi.LocalId
                       && meteo.Data_Leitura >= Data_Leiturai
                       && meteo.Data_Leitura <= Data_Leituraf
                       )
                       select new MeteorologiaDTO()
                       {
                           Id = meteo.Id,
                           Data_Leitura = meteo.Data_Leitura,
                           Hora_Leitura = meteo.Hora_Leitura,
                           Temp = meteo.Temp,
                           Vento = meteo.Vento,
                           Humidade = meteo.Humidade,
                           Pressão = meteo.Pressão,
                           No = meteo.No,
                           No2 = meteo.No2,
                           LocalId = meteo.LocalId,
                           NomeLocal = meteo.Local.Nome,
                           GPS_Lat = meteo.Local.GPS_Lat,
                           GPS_Long = meteo.Local.GPS_Long
                       };


            return mets.AsQueryable();

        }

        // GET: api/Meteorologias    ex:/api/Meteorologias?PoiID=1&Data_Leitura=‎2016-11-10&Hora_Leitura=12:00&Margem=49
        [Authorize(Roles = "Editor")]
        [ResponseType(typeof(MeteorologiaDTO))]
        public IQueryable<MeteorologiaDTO> GetMeteorologia(int PoiId, DateTime Data_Leitura, TimeSpan Hora_Leitura, int Margem)
        {


            //tirar o ID do local pelo POI
            Poi poi = db.Pois.Where(p => p.Id == PoiId).Single();

            int codLocal = poi.LocalId;


            TimeSpan margemHoras = TimeSpan.FromHours(Margem);

            DateTime diaMin = constroiDiaMin(Data_Leitura, Hora_Leitura, margemHoras);
            DateTime diaMax = constroiDiaMax(Data_Leitura, Hora_Leitura, margemHoras);

            TimeSpan horaMin = constroiHoraMin(Data_Leitura, Hora_Leitura, margemHoras);
            TimeSpan horaMax = constroiHoraMax(Data_Leitura, Hora_Leitura, margemHoras);

            if (diaMin != Data_Leitura || diaMax != Data_Leitura)
            {
                return filtraIntervaloDatas(codLocal, Data_Leitura, diaMin, diaMax, horaMin, horaMax);
            }
            else
            {
                return filtra1Dia(codLocal, Data_Leitura, horaMin, horaMax);
            }

        }

        private IQueryable<MeteorologiaDTO> filtraIntervaloDatas(int codLocal, DateTime Data_Leitura, DateTime diaMin, DateTime diaMax, TimeSpan horaMin, TimeSpan horaMax)
        {
            //filtragem;
            var mets = from meteo in db.Meteorologias.ToList()
                       where (meteo.LocalId == codLocal
                       && meteo.Data_Leitura >= diaMin
                       && meteo.Data_Leitura <= diaMax
                       )
                       select new MeteorologiaDTO()
                       {
                           Id = meteo.Id,
                           Data_Leitura = meteo.Data_Leitura,
                           Hora_Leitura = meteo.Hora_Leitura,
                           Temp = meteo.Temp,
                           Vento = meteo.Vento,
                           Humidade = meteo.Humidade,
                           Pressão = meteo.Pressão,
                           No = meteo.No,
                           No2 = meteo.No2,
                           LocalId = meteo.LocalId,
                           NomeLocal = meteo.Local.Nome,
                           GPS_Lat = meteo.Local.GPS_Lat,
                           GPS_Long = meteo.Local.GPS_Long
                       };


            List<MeteorologiaDTO> listaRetorno = new List<MeteorologiaDTO>();

            DateTime tempoMin = diaMin.Add(horaMin);
            DateTime tempoMax = diaMax.Add(horaMax);


            foreach (var m in mets)
            {
                DateTime tempoIter = m.Data_Leitura.Add(m.Hora_Leitura);

                if (tempoMin <= tempoIter && tempoIter <= tempoMax)
                {
                    listaRetorno.Add(m);

                }

            }

            return listaRetorno.AsQueryable();
        }

        private IQueryable<MeteorologiaDTO> filtra1Dia(int codLocal, DateTime Data_Leitura, TimeSpan horaMin, TimeSpan horaMax)
        {
            //filtragem;
            var mets = from meteo in db.Meteorologias
                       where (meteo.LocalId == codLocal
                       && meteo.Data_Leitura == Data_Leitura
                       && meteo.Hora_Leitura >= horaMin
                       && meteo.Hora_Leitura <= horaMax
                       )
                       select new MeteorologiaDTO()
                       {
                           Id = meteo.Id,
                           Data_Leitura = meteo.Data_Leitura,
                           Hora_Leitura = meteo.Hora_Leitura,
                           Temp = meteo.Temp,
                           Vento = meteo.Vento,
                           Humidade = meteo.Humidade,
                           Pressão = meteo.Pressão,
                           No = meteo.No,
                           No2 = meteo.No2,
                           LocalId = meteo.LocalId,
                           NomeLocal = meteo.Local.Nome,
                           GPS_Lat = meteo.Local.GPS_Lat,
                           GPS_Long = meteo.Local.GPS_Long
                       };
            return mets;
        }

        private DateTime constroiDiaMin(DateTime Data_Leitura, TimeSpan Hora_Leitura, TimeSpan margemHoras)
        {
            DateTime diaMin = Data_Leitura.Add(Hora_Leitura - margemHoras);

            DateTime retorno = new DateTime(diaMin.Year, diaMin.Month, diaMin.Day);
            return retorno;

        }

        private DateTime constroiDiaMax(DateTime Data_Leitura, TimeSpan Hora_Leitura, TimeSpan margemHoras)
        {
            DateTime diaMax = Data_Leitura.Add(Hora_Leitura + margemHoras);

            DateTime retorno = new DateTime(diaMax.Year, diaMax.Month, diaMax.Day);
            return retorno;

        }


        private TimeSpan constroiHoraMin(DateTime Data_Leitura, TimeSpan Hora_Leitura, TimeSpan margemHoras)
        {
            DateTime diaMin = Data_Leitura.Add(Hora_Leitura - margemHoras);

            TimeSpan horaMin = new TimeSpan(diaMin.Hour, diaMin.Minute, diaMin.Second);

            return horaMin;

        }

        private TimeSpan constroiHoraMax(DateTime Data_Leitura, TimeSpan Hora_Leitura, TimeSpan margemHoras)
        {
            DateTime diaMax = Data_Leitura.Add(Hora_Leitura + margemHoras);

            TimeSpan horaMax = new TimeSpan(diaMax.Hour, diaMax.Minute, diaMax.Second);


            return horaMax;
        }


        // GET: api/Meteorologias    ex:/api/Meteorologias?PoiID=1&Data_Leitura=2016-09-01&Hora_Leitura=15:30&Margem=3
        [Authorize(Roles = "Editor")]
        [ResponseType(typeof(MeteorologiaDTO))]
        public IQueryable<MeteorologiaDTO> GetMeteorologia(string NomeLocal)
        {

            //tirar o ID do local pelo POI
            string[] words = NomeLocal.Split(',');

            List<MeteorologiaDTO> listadeMeteos = new List<MeteorologiaDTO>();

            foreach (string nome in words)
            {
                Local Local2 = db.Locais.FirstOrDefault(p => p.Nome.Equals(nome));
                if (Local2 != null)
                {


                    int codLocal = Local2.Id;


                    //filtragem;
                    var mets = from meteo in db.Meteorologias
                               where (meteo.LocalId == codLocal
                               )
                               select new MeteorologiaDTO()
                               {
                                   Id = meteo.Id,
                                   Data_Leitura = meteo.Data_Leitura,
                                   Hora_Leitura = meteo.Hora_Leitura,
                                   Temp = meteo.Temp,
                                   Vento = meteo.Vento,
                                   Humidade = meteo.Humidade,
                                   Pressão = meteo.Pressão,
                                   No = meteo.No,
                                   No2 = meteo.No2,
                                   LocalId = meteo.LocalId,
                                   NomeLocal = meteo.Local.Nome,
                                   GPS_Lat = meteo.Local.GPS_Lat,
                                   GPS_Long = meteo.Local.GPS_Long
                               };

                    foreach (var m in mets)
                    {
                        listadeMeteos.Add(m);

                    }
                }
            }
            return listadeMeteos.AsQueryable();
        }



        private TimeSpan constroiHoraMin(TimeSpan Hora_Leitura, TimeSpan margemHoras)
        {
            TimeSpan horaMinPossivelDia = TimeSpan.FromHours(0);
            TimeSpan horaMin = Hora_Leitura.Subtract(margemHoras);
            if (horaMin.Hours < 0 || horaMin.TotalHours < 0)
            {
                return horaMinPossivelDia;
            }
            return horaMin;

        }

        private TimeSpan constroiHoraMax(TimeSpan Hora_Leitura, TimeSpan margemHoras)
        {
            TimeSpan horaMaxPossivelDoDia = TimeSpan.FromHours(23).Add(TimeSpan.FromMinutes(59)).Add(TimeSpan.FromSeconds(59));

            TimeSpan horaMax = Hora_Leitura.Add(margemHoras);
            if (horaMax.TotalHours > 24)
            {
                return horaMaxPossivelDoDia;
            }
            return horaMax;

        }
        [Authorize(Roles = "Editor")]
        [System.Web.Http.Route("Facetas/Local")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage LocaisComMeteorologias()
        {

            //todas meteorologias
            var meteos = from meteo in db.Meteorologias
                         select new MeteorologiaDTO()
                         {
                             Id = meteo.Id,
                             Data_Leitura = meteo.Data_Leitura,
                             Hora_Leitura = meteo.Hora_Leitura,
                             Temp = meteo.Temp,
                             Vento = meteo.Vento,
                             Humidade = meteo.Humidade,
                             Pressão = meteo.Pressão,
                             No = meteo.No,
                             No2 = meteo.No2,
                             LocalId = meteo.LocalId,
                             NomeLocal = meteo.Local.Nome,
                             GPS_Lat = meteo.Local.GPS_Lat,
                             GPS_Long = meteo.Local.GPS_Long
                         };


            var locs = "[";
            var results = (from loc in meteos
                           select loc.NomeLocal).Distinct().ToList();

            foreach (var l in results)
            {

                locs = locs + "\"" + l + "\",";

            }
            locs = locs.Remove(locs.LastIndexOf(",")) + "]";


            return new HttpResponseMessage()
            {
                Content = new StringContent(locs, System.Text.Encoding.UTF8, "text/json")
            };

        }


        // PUT: api/Meteorologias/5
        [Authorize(Roles = "Editor")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMeteorologia(int id, Meteorologia meteorologia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != meteorologia.Id)
            {
                return BadRequest();
            }

            db.Entry(meteorologia).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MeteorologiaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [Authorize(Roles = "Editor")]
        // POST: api/Meteorologias
        [ResponseType(typeof(Meteorologia))]
        public async Task<IHttpActionResult> PostMeteorologia(Meteorologia meteorologia)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Meteorologias.Add(meteorologia);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = meteorologia.Id }, meteorologia);
        }

        // DELETE: api/Meteorologias/5
        [Authorize(Roles = "Editor")]
        [ResponseType(typeof(Meteorologia))]
        public async Task<IHttpActionResult> DeleteMeteorologia(int id)
        {
            Meteorologia meteorologia = await db.Meteorologias.FindAsync(id);
            if (meteorologia == null)
            {
                return NotFound();
            }

            db.Meteorologias.Remove(meteorologia);
            await db.SaveChangesAsync();

            return Ok(meteorologia);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MeteorologiaExists(int id)
        {
            return db.Meteorologias.Count(e => e.Id == id) > 0;
        }

        [Authorize(Roles = "Editor")]
        [System.Web.Http.Route("Facetas/min")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage continuoMin(string faceta)
        {
            var min = "[{\"min\":\"";

            switch (faceta)
            {
                case "Temp":
                    min += db.Meteorologias.Min(c => c.Temp).ToString();
                    break;
                case "Vento":
                    min += db.Meteorologias.Min(c => c.Vento).ToString();
                    break;
                case "Humidade":
                    min += db.Meteorologias.Min(c => c.Humidade).ToString();
                    break;
                case "Pressão":
                    min += db.Meteorologias.Min(c => c.Pressão).ToString();
                    break;
                case "No":
                    min += db.Meteorologias.Min(c => c.No).ToString();
                    break;
                case "No2":
                    min += db.Meteorologias.Min(c => c.No2).ToString();
                    break;
                case "GPS_Lat":
                    min += db.Meteorologias.Min(c => c.Local.GPS_Lat).ToString();
                    break;
                case "GPS_Long":
                    min += db.Meteorologias.Min(c => c.Local.GPS_Long).ToString();
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }

            min += "\"}]";

            return new HttpResponseMessage()
            {
                Content = new StringContent(min, System.Text.Encoding.UTF8, "text/json")
            };

        }

        [Authorize(Roles = "Editor")]
        [System.Web.Http.Route("Facetas/max")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage continuoMax(string faceta)
        {
            var max = "[{\"max\":\"";

            switch (faceta)
            {
                case "Temp":
                    max += db.Meteorologias.Max(c => c.Temp).ToString();
                    break;
                case "Vento":
                    max += db.Meteorologias.Max(c => c.Vento).ToString();
                    break;
                case "Humidade":
                    max += db.Meteorologias.Max(c => c.Humidade).ToString();
                    break;
                case "Pressão":
                    max += db.Meteorologias.Max(c => c.Pressão).ToString();
                    break;
                case "No":
                    max += db.Meteorologias.Max(c => c.No).ToString();
                    break;
                case "No2":
                    max += db.Meteorologias.Max(c => c.No2).ToString();
                    break;
                case "GPS_Lat":
                    max += db.Meteorologias.Max(c => c.Local.GPS_Lat).ToString();
                    break;
                case "GPS_Long":
                    max += db.Meteorologias.Max(c => c.Local.GPS_Long).ToString();
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }

            max += "\"}]";

            return new HttpResponseMessage()
            {
                Content = new StringContent(max, System.Text.Encoding.UTF8, "text/json")
            };

        }

        [Authorize(Roles = "Editor")]
        [System.Web.Http.Route("Facetas/IdSensor")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage FacetaIdSensor()
        {
            string dadosSensor = "";

            dadosSensor = dadosSensor + "<sensores>" +
            "<sensor id = '0'>" +
            "<nome>Meteorologia</nome>" +
            "<descricao>Estação meteorologica</descricao>" +
            "</sensor>" +
            "</sensores>";

            return new HttpResponseMessage()
            {
                Content = new StringContent(dadosSensor, System.Text.Encoding.UTF8, "text/xml")
            };
        }

        [Authorize(Roles = "Editor")]
        [System.Web.Http.Route("Facetas")]
        [System.Web.Http.HttpGet]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage FacetasXML()
        {
            string dadosFaceta = "";

            dadosFaceta = "<Facetas>" +
                "<Faceta id = '1'>" +
                    "<CampoBD>Data_Leitura</CampoBD>" +
                    "<Nome>Data de leitura</Nome>" +
                    "<Grandeza>contínuo</Grandeza>" +
                    "<Tipo>data</Tipo>" +
                    "<Semantica>data</Semantica>" +
                "</Faceta>" +
                "<Faceta id ='2'>" +
                    "<CampoBD>Hora_Leitura</CampoBD>" +
                    "<Nome>Hora_Leitura</Nome>" +
                    "<Grandeza>contínuo</Grandeza>" +
                    "<Tipo>hora</Tipo>" +
                    "<Semantica>hora</Semantica>" +
                "</Faceta>" +
                "<Faceta id ='4'>" +
                    "<CampoBD>Temp</CampoBD>" +
                    "<Nome>Temp (ºC)</Nome>" +
                    "<Grandeza>contínuo</Grandeza>" +
                    "<Tipo>numérico</Tipo>" +
                    "<Semantica>temperatura</Semantica>" +
                "</Faceta>" +
                "<Faceta id ='5'>" +
                    "<CampoBD>Vento</CampoBD>" +
                    "<Nome>Vento</Nome>" +
                    "<Grandeza>contínuo</Grandeza>" +
                    "<Tipo>numérico</Tipo>" +
                    "<Semantica>vento</Semantica>" +
                "</Faceta>" +
                "<Faceta id ='6'>" +
                    "<CampoBD>Humidade</CampoBD>" +
                    "<Nome>Humidade</Nome>" +
                    "<Grandeza>contínuo</Grandeza>" +
                    "<Tipo>numérico</Tipo>" +
                    "<Semantica>humidade</Semantica>" +
                "</Faceta>" +
                "<Faceta id ='7'>" +
                    "<CampoBD>Pressão</CampoBD>" +
                    "<Nome>Pressão</Nome>" +
                    "<Grandeza>contínuo</Grandeza>" +
                    "<Tipo>numérico</Tipo>" +
                    "<Semantica>pressão</Semantica>" +
                "</Faceta>" +
                "<Faceta id ='8'>" +
                    "<CampoBD>No</CampoBD>" +
                    "<Nome>NO</Nome>" +
                    "<Grandeza>contínuo</Grandeza>" +
                    "<Tipo>numérico</Tipo>" +
                    "<Semantica>no</Semantica>" +
                "</Faceta>" +
                "<Faceta id ='9'>" +
                    "<CampoBD>No2</CampoBD>" +
                    "<Nome>NO2</Nome>" +
                    "<Grandeza>contínuo</Grandeza>" +
                    "<Tipo>numérico</Tipo>" +
                    "<Semantica>no2</Semantica>" +
                "</Faceta>" +
                "<Faceta id = '10'>" +
                    "<CampoBD>NomeLocal</CampoBD>" +
                    "<Nome>Local</Nome>" +
                    "<Grandeza>discreto</Grandeza>" +
                    "<Tipo>alfanumérico</Tipo>" +
                    "<Semantica>localização</Semantica>" +
                "</Faceta>" +
                "<Faceta id = '11'>" +
                    "<CampoBD>GPS_Lat</CampoBD>" +
                    "<Nome>GPS Latitude</Nome>" +
                    "<Grandeza>contínuo</Grandeza>" +
                    "<Tipo>numérico</Tipo>" +
                    "<Semantica>coordenadas</Semantica>" +
                "</Faceta>" +
                "<Faceta id = '12'>" +
                    "<CampoBD>GPS_Long</CampoBD>" +
                    "<Nome>GPS Longitude</Nome>" +
                    "<Grandeza>contínuo</Grandeza>" +
                    "<Tipo>numérico</Tipo>" +
                    "<Semantica>coordenadas</Semantica>" +
                "</Faceta>" +
                "</Facetas>";
            //return dadosFaceta;
            return new HttpResponseMessage()
            {
                Content = new StringContent(dadosFaceta, System.Text.Encoding.UTF8, "text/xml")
            };

        }
    }
}