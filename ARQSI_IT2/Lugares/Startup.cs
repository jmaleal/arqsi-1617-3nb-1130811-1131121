﻿using Microsoft.Owin;
using Owin;
using System;
using System.IO;
using System.Threading;

[assembly: OwinStartupAttribute(typeof(Lugares.Startup))]
namespace Lugares
{
    public partial class Startup
    {
        public string AttachDBFilme { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            ////Acrescentei para podermos usar o caminho relativo a partir do projeto atual
            //var asmPath = Thread.GetDomain().BaseDirectory;

            //var parent1 = Directory.GetParent(asmPath).ToString();
            //var parent2 = Directory.GetParent(parent1).ToString();
            //var parent3 = Directory.GetParent(parent2).ToString();
            ////var mdfPath = Path.Combine(parent3, "ARQSI_IT2\\Lugares\\", "App_Data");
            //var mdfPath = Path.Combine(parent3, "ARQSI_IT2\\DB\\", "App_Data");

            //AppDomain.CurrentDomain.SetData("DataDirectory", mdfPath);
        }

    }
    }

