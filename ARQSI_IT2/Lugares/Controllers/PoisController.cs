﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClassLibrary.DAL;
using ClassLibrary.Model;
using Microsoft.AspNet.Identity;

namespace Lugares.Controllers
{
    [RequireHttps] //colocar aqui = colocar em todos os métodos
    public class PoisController : Controller
    {
        //private PoiDBContext db = new PoiDBContext();
        private PoiRepository repo = new PoiRepository(new LocalDBContext());


        private Boolean checkLoggedInUser()
        {
            var userLoggedIn = User.Identity.GetUserName();
            if (userLoggedIn == null || userLoggedIn.Equals(""))
            {
                return false;
            }
            return true;
        }



        // GET: Pois
        public ActionResult Index()
        {
            //var pois = db.Pois.Include(p => p.Local);
            //return View(pois.ToList());
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }
            var pois = repo.getContext().Pois.Include(p => p.Local);
            return View(pois.ToList());
        }

        // GET: Pois/Details/5
        public ActionResult Details(int? id)
        {
            //verifica utilizador
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            // Poi poi = db.Pois.Find(id);
            Poi poi = repo.GetbyId((int)id);
            if (poi == null)
            {
                return HttpNotFound();
            }
            return View(poi);
        }

        // GET: Pois/Create
        public ActionResult Create()
        {
            //verifica utilizador
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }
            ViewBag.LocalId = new SelectList(repo.getContext().Locais, "Id", "Nome");
            return View();
        }

        // POST: Pois/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nome,Descrição,LocalId")] Poi poi)
        {
            //verifica utilizador
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }

            if (ModelState.IsValid)
            {
                // db.Pois.Add(poi);
                // db.SaveChanges();
                poi.Creator= User.Identity.GetUserName();
                repo.Create(poi);
                return RedirectToAction("Index");
            }

            ViewBag.LocalId = new SelectList(repo.getContext().Locais, "Id", "Nome", poi.LocalId);
            return View(poi);
        }

        // GET: Pois/Edit/5
        public ActionResult Edit(int? id)
        {

            //verifica utilizador
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Poi poi = db.Pois.Find(id);
            Poi poi = repo.GetbyId((int)id);
            if (poi == null)
            {
                return HttpNotFound();
            }

            if (!poi.Creator.Equals(User.Identity.GetUserName())) //se não é o criador do Poi não deixa avançar
            {
                return Content("Just the Creator can Edit Poi!");
            }

            ViewBag.LocalId = new SelectList(repo.getContext().Locais, "Id", "Nome", poi.LocalId);
            return View(poi);
        }

        // POST: Pois/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nome,Descrição,LocalId,Creator")] Poi poi)
        {
            //verifica utilizador
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }

            if (ModelState.IsValid)
            {
                //db.Entry(poi).State = EntityState.Modified;
                //db.SaveChanges();
                repo.Update(poi);
                return RedirectToAction("Index");
            }
            ViewBag.LocalId = new SelectList(repo.getContext().Locais, "Id", "Nome", poi.LocalId);
            return View(poi);
        }

        // GET: Pois/Delete/5
        public ActionResult Delete(int? id)
        {

            //verifica utilizador
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Poi poi = db.Pois.Find(id);
            Poi poi = repo.GetbyId((int)id);
            if (poi == null)
            {
                return HttpNotFound();
            }

            if (!poi.Creator.Equals(User.Identity.GetUserName())) //se não é o criador do Poi não deixa avançar
            {
                return Content("Just the Creator can Edit Poi!");
            }


            return View(poi);
        }

        // POST: Pois/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //verifica utilizador
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }

            // Poi poi = db.Pois.Find(id);
            // db.Pois.Remove(poi);
            // db.SaveChanges();
            Poi poi = repo.GetbyId((int)id);
            repo.Delete(poi);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
                repo.dispose();
            }
            base.Dispose(disposing);
        }
    }
}
