﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClassLibrary.DAL;
using ClassLibrary.Model;
using Microsoft.AspNet.Identity;

namespace Lugares.Controllers
{
    [RequireHttps] //colocar aqui = colocar em todos os métodos
    public class LocalsController : Controller
    {
        //private LocalDBContext db = new LocalDBContext();

        private LocalRepository repo = new LocalRepository(new LocalDBContext());

        private Boolean checkLoggedInUser()
        {
            var userLoggedIn = User.Identity.GetUserName();
            if (userLoggedIn == null || userLoggedIn.Equals(""))
            {
                return false;
            }
            return true;
        }



        // GET: Locals
        public ActionResult Index()
        {
            // return View(db.Locais.ToList());


            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            return View(repo.List());
        }

        // GET: Locals/Details/5
        public ActionResult Details(int? id)
        {
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Local local = db.Locais.Find(id);
            Local local = repo.GetbyId((int)id);
            if (local == null)
            {
                return HttpNotFound();
            }
            return View(local);
        }

        // GET: Locals/Create
        public ActionResult Create()
        {
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            return View();
        }

        // POST: Locals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nome,GPS_Lat,GPS_Long")] Local local)
        {
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }

            if (ModelState.IsValid)
            {
                //db.Locais.Add(local);
                //db.SaveChanges();
                repo.Create(local);
                return RedirectToAction("Index");
            }

            return View(local);
        }

        // GET: Locals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Local local = db.Locais.Find(id);
            Local local = repo.GetbyId((int)id);
            if (local == null)
            {
                return HttpNotFound();
            }
            return View(local);
        }

        // POST: Locals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nome,GPS_Lat,GPS_Long")] Local local)
        {
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            if (ModelState.IsValid)
            {
                //db.Entry(local).State = EntityState.Modified;
                // db.SaveChanges();
                repo.Update(local);
                return RedirectToAction("Index");
            }
            return View(local);
        }

        // GET: Locals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            // Local local = db.Locais.Find(id);
            Local local = repo.GetbyId((int)id);
            if (local == null)
            {
                return HttpNotFound();
            }
            return View(local);
        }

        // POST: Locals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            //Local local = db.Locais.Find(id);
            //db.Locais.Remove(local);
            //db.SaveChanges();
            Local local = repo.GetbyId((int)id);
            repo.Delete(local);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
                repo.dispose();
            }
            base.Dispose(disposing);
        }
    }
}
