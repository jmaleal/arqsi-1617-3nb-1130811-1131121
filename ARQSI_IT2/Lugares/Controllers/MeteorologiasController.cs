﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClassLibrary.DAL;
using ClassLibrary.Model;
using Microsoft.AspNet.Identity;

namespace Lugares.Controllers
{
    [RequireHttps] //colocar aqui = colocar em todos os métodos
    public class MeteorologiasController : Controller
    {
        //private MeteorologiaDBContext db = new MeteorologiaDBContext();
        private MeteorologiaRepository repo = new MeteorologiaRepository(new LocalDBContext());

        private Boolean checkLoggedInUser()
        {
            var userLoggedIn = User.Identity.GetUserName();
            if (userLoggedIn == null || userLoggedIn.Equals(""))
            {
                return false;
            }
            return true;
        }

        // GET: Meteorologias
        public ActionResult Index()
        {
            //var meteorologias = db.Meteorologias.Include(m => m.Local);
            //return View(db.meteorologias.ToList();


            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            var meteorologias = repo.getContext().Meteorologias.Include(p => p.Local);
            return View(meteorologias.ToList());
        }

        // GET: Meteorologias/Details/5
        public ActionResult Details(int? id)
        {

            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Meteorologia meteorologia = db.Meteorologias.Find(id);
            Meteorologia meteorologia = repo.GetbyId((int)id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            return View(meteorologia);
        }

        // GET: Meteorologias/Create
        public ActionResult Create()
        {

            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            ViewBag.LocalId = new SelectList(repo.getContext().Locais, "Id", "Nome");
            return View();
        }

        // POST: Meteorologias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Data_Leitura,Hora_Leitura,Temp,Vento,Humidade,Pressão,No,No2,LocalId")] Meteorologia meteorologia)
        {

            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            if (ModelState.IsValid)
            {
                //db.Meteorologias.Add(meteorologia);
                //db.SaveChanges();
                repo.Create(meteorologia);
                return RedirectToAction("Index");
            }

            ViewBag.LocalId = new SelectList(repo.getContext().Locais, "Id", "Nome", meteorologia.LocalId);
            return View(meteorologia);
        }

        // GET: Meteorologias/Edit/5
        public ActionResult Edit(int? id)
        {

            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Meteorologia meteorologia = db.Meteorologias.Find(id);
            Meteorologia meteorologia = repo.GetbyId((int)id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocalId = new SelectList(repo.getContext().Locais, "Id", "Nome", meteorologia.LocalId);
            return View(meteorologia);
        }

        // POST: Meteorologias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Data_Leitura,Hora_Leitura,Temp,Vento,Humidade,Pressão,No,No2,LocalId")] Meteorologia meteorologia)
        {

            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            if (ModelState.IsValid)
            {
                //db.Entry(meteorologia).State = EntityState.Modified;
                //db.SaveChanges();
                repo.Update(meteorologia);
                return RedirectToAction("Index");
            }
            ViewBag.LocalId = new SelectList(repo.getContext().Locais, "Id", "Nome", meteorologia.LocalId);
            return View(meteorologia);
        }

        // GET: Meteorologias/Delete/5
        public ActionResult Delete(int? id)
        {

            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Meteorologia meteorologia = db.Meteorologias.Find(id);
            Meteorologia meteorologia = repo.GetbyId((int)id);
            if (meteorologia == null)
            {
                return HttpNotFound();
            }
            return View(meteorologia);
        }

        // POST: Meteorologias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            if (!checkLoggedInUser())
            {
                return Content("U must log in first");
            }

            //Meteorologia meteorologia = db.Meteorologias.Find(id);
            //db.Meteorologias.Remove(meteorologia);
            //db.SaveChanges();
            Meteorologia meteorologia = repo.GetbyId((int)id);
            repo.Delete(meteorologia);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
                repo.dispose();
            }
            base.Dispose(disposing);
        }
    }
}
